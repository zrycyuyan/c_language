#define _CRT_SECURE_NO_WARNINGS
#include"SeqList.h"

//测试顺序表
void SLtest()
{
	SL s1;
	SLInit(&s1);
	SLPushBack(&s1, 1);
	SLPushBack(&s1, 2);
	SLPushBack(&s1, 3);
	SLPushBack(&s1, 4);
	SLPrint(&s1);

	SLPushFront(&s1, 7);
	SLPushFront(&s1, 8);
	SLPushFront(&s1, 9);
	SLPrint(&s1);

	SLPopBack(&s1);
	SLPrint(&s1);

	SLPopFront(&s1);
	SLPrint(&s1);

	SLInsert(&s1, 1, 100);
	SLPrint(&s1);

	SLErase(&s1, 1);
	SLPrint(&s1);

	if (SLFind(&s1,8) == true)
		printf("找到了！\n");
	else
		printf("找不到！\n");

	SLDestory(&s1);
}
int main()
{
	SLtest();
	return 0;
}