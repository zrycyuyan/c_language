#define _CRT_SECURE_NO_WARNINGS
#include"SeqList.h"

//对顺序表初始化
void SLInit(SL* ps)
{
	ps->a = NULL;
	ps->size = ps->capacity = 0;	
}



//释放顺序表的内容
void SLDestory(SL* ps)
{
	if (ps->a)
	{
		free(ps->a);
		ps->a = NULL;
	}
	ps->size = ps->capacity = 0;
}



//检查空间是否足够，若不够就扩容的函数
void SLCheckcapacity(SL* ps)
{
	if (ps->size == ps->capacity)//没有剩余空间再插入一个数据了       要扩容
	{
		int newCapcity = ps->capacity == 0? 4: 2 * ps->capacity;//先判断原来空间是不是为0，若是先对原来空间给4个大小在扩容；若不是，则直接扩容原来的两倍
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newCapcity * sizeof(SLDataType));
		if (tmp == NULL)
		{
			perror("relloc!");
			return 1;
		}
		ps->a = tmp;
		tmp = NULL;
		ps->capacity = newCapcity;//将空间大小改为扩容后的空间大小
	}
}



//尾插
void SLPushBack(SL* ps,SLDataType x)
{
	//先判断ps是否为空
	//暴力方式
	//assert(ps);
	//柔和方式
	if (ps == NULL)
	{
		perror("struct1!");
		return 1;
	}

	//对空间进行检查是否足够，若够直接尾插，若不够则先扩容再尾插
	SLCheckcapacity(ps);

	//将数据进行尾插，同时有效空间数也要增加
	/*ps->a[ps->size] = x;
	ps->size++;*/
	//上面这个可写成下面这种形式
	ps->a[ps->size++] = x;
}



//头插
void SLPushFront(SL* ps, SLDataType x)
{
	if (ps == NULL)
	{
		perror("struct2!");
		return 1;
	}

	SLCheckcapacity(ps);

	//空间足够后，将每个数据后移一位
	for (int i = ps->size; i > 0; i--)
	{
		ps->a[i] = ps->a[i - 1];
	}

	ps->a[0] = x;
	ps->size++;
}



//尾删
void SLPopBack(SL* ps)
{
	if (ps == NULL)
	{
		perror("struct3!");
		return 0;
	}

	assert(!SLIsEmpty(ps));

	//直接将有效空间向前移一位即可
	ps->size--;
}



//头删
void SLPopFront(SL* ps)
{
	assert(ps);
	assert(!SLIsEmpty(ps));
	//后边的数据都往前移一位
	for (int i = 0; i < ps->size - 1; i++)
	{
		ps->a[i] = ps->a[i + 1];
	}
	ps->size--;
}



//打印函数
void SLPrint(SL* ps)
{
	for (int  i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}



//判断有效空间是否足够的函数
//因为当你有效空间减到零以下时肯定不行
bool SLIsEmpty(SL* ps)
{
	assert(ps);
	return ps->size == 0;
}



//从中间任意位置插入数据的函数
//pos表示距离起始位置的偏移量，从0开始
void SLInsert(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->capacity);//这个偏移量要有条件限制

	//检查空间是否足够，若不够则扩容
	SLCheckcapacity(ps);

	//将pos后的数据从后往前移
	for (int i = ps->size; i > pos; i--)
	{
		ps->a[i] = ps->a[i - 1];
	}

	ps->a[pos] = x;
	ps->size++;
}



//从中间任意位置删除数据
void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(!SLIsEmpty(ps));//删除时肯定要判断有效空间够不够删
	assert(pos >= 0 && pos <= ps->capacity);

	//将pos后的数据从前往后向前移一位
	for (int i = pos; i < ps->size - 1; i++)
	{
		ps->a[i] = ps->a[i + 1];
	}

	ps->size--;
}



//查找顺序表中数据是否存在
bool SLFind(SL* ps, SLDataType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			//找到了
			return true;
		}
	}
	return false;
}