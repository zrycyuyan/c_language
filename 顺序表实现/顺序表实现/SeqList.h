#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<assert.h>
#include<stdbool.h>
#include<stdlib.h>


typedef int SLDataType;//将类型重命名一下方便后续要改类型时，只需在这里改一下就行
//定义一个动态顺序表
typedef struct SeqList
{
	SLDataType* a;
	int size;//有效数据大小
	int capacity;//空间大小
}SL;



//对顺序表进行初始化
void SLInit(SL* ps);
void SLDestory(SL* ps);//顺序表的销毁



//对顺序表进行头部/尾部 插入/删除
void SLPushBack(SL* ps,SLDataType x);//尾插
void SLPushFront(SL* ps, SLDataType x);//头插
void SLPopBack(SL* ps);//尾删
void SLPopFront(SL* ps);//头删


//从中间任意位置插入数据
void SLInsert(SL* ps, int pos, SLDataType x);


//从中间任意位置删除数据
void SLErase(SL* ps,int pos);

//打印函数
void SLPrint(SL* ps);



//判断顺序表的有效空间是否为空的函数
bool SLIsEmpty(SL* ps);



//查找顺序表中数据是否存在
bool SLFind(SL* ps, SLDataType x);