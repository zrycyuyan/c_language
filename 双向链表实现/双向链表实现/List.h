#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>


//类型重命名
typedef int LTDataType;
//定义双向链表的节点结构
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}LTNode;



//对双向链表初始化（注意我们实现的链表是双向循环带头链表，有一个哨兵位，不存储任何有效数据，所以需要初始化，我们在前面实现的是单项不循环不带头链表，没有哨兵位，每一个节点都存储有效数据，所以无需初始化，只需定义一个相应指针变量循环访问即可，这里可以将它们好好比较一下）
//void LTInit(LTNode** pphead);//初始化方法一，传一个二级指针
LTNode* LTInit();//初始化方法二，不需传任何参数，返回一个已初始化好的哨兵位地址
//我们这里选初始化法二更好



//打印链表中数据
void LTPrint(LTNode* phead);



//尾插
void LTPushBack(LTNode* phead,LTDataType x);
//头插
void LTPushFront(LTNode* phead, LTDataType x);
//尾删
void LTPopBack(LTNode* phead);
//头删
void LTPopFront(LTNode* phead);

//在指定位置pos后插入数据
void LTInsert(LTNode* pos, LTDataType x);
//查找指定位置的数据
LTNode* LTFind(LTNode* phead, LTDataType x);
//删除指定位置的数据
void LTErase(LTNode* pos);

//销毁链表
void LTDestroy(LTNode* phead);
