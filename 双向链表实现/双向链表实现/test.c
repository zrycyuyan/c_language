#define _CRT_SECURE_NO_WARNINGS
#include"List.h"

void ListTest()
{
	/*LTNode* plist;
	LTInit(&plist);*/

	
	LTNode* plist =LTInit();
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);
	LTPrint(plist);

	LTPushFront(plist, 7);
	LTPushFront(plist, 8);
	LTPushFront(plist, 9);
	LTPrint(plist);

	LTPopBack(plist);
	LTPopBack(plist);
	LTPopBack(plist);
	LTPrint(plist);

	LTPopFront(plist);
	LTPopFront(plist);
	LTPopFront(plist);
	LTPrint(plist);

	LTInsert(LTFind(plist, 1), 10);
	LTPrint(plist);

	LTErase(LTFind(plist, 1));
	LTPrint(plist);

	LTDestroy(plist);
}
int main()
{
	ListTest();
	return 0;
}