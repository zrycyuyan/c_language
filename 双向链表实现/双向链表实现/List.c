#define _CRT_SECURE_NO_WARNINGS
#include"List.h"



//初始化方法一，传一个二级指针
//void LTInit(LTNode** pphead)
//{
//	assert(pphead);
//
//	//开辟哨兵位节点
//	*pphead = (LTNode*)malloc(sizeof(LTNode));
//	if (*pphead == NULL)
//	{
//		perror("malloc");
//		return;
//	}
//
//	//对哨兵位节点进行初始化
//	(*pphead)->data = -1;
//	(*pphead)->next = (*pphead)->prev = *pphead;
//}



//初始化方法二，不需传任何参数，返回一个已初始化好的哨兵位地址
LTNode* LTInit()
{//实现逻辑与上面初始化法一一样
	LTNode* phead = (LTNode*)malloc(sizeof(LTNode));

	if (phead == NULL)
	{
		perror("LTInit()::malloc()");
		return NULL;
	}

	phead->data = -1;
	phead->next = phead->prev = phead;
	return phead;
}



//申请一个新节点存储x的值
LTNode* ListBuyNode(LTDataType x)
{
	LTNode* node = (LTNode*)malloc(sizeof(LTNode));
	if (node == NULL)
	{
		perror("ListBuyNode()::malloc()");
		return;
	}

	node->data = x;
	node->next = node->prev = node;
	return node;
}


//尾插
void LTPushBack(LTNode* phead,LTDataType x)
{
	assert(phead);
	LTNode* node = ListBuyNode(x);

	//将新节点node与链表连接起来
	node->prev = phead->prev;
	node->next = phead;

	//链表头尾节点要指向node
	phead->prev->next = node;
	phead->prev = node;
}



//打印链表中数据
void LTPrint(LTNode* phead)
{
	LTNode* cur = phead->next;//因为phead为哨兵位不存储有效数据

	while (cur != phead)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("\n");
}



//头插
void LTPushFront(LTNode* phead, LTDataType x)
{
	assert(phead);
	LTNode* node = ListBuyNode(x);

	//node节点的 prev next
	node->prev = phead;
	node->next = phead->next;

	//处理phead  phead->next
	phead->next->prev = node;
	phead->next = node;
}



//尾删
void LTPopBack(LTNode* phead)
{
	assert(phead);
	//这个链表除了哨兵位节点还要有有效节点，不能一个有效节点都没有
	assert(phead->next != phead);

	//记录下要删的尾节点
	LTNode* del = phead->prev;

	//将新的尾节点与哨兵位相互连接起来
	del->prev->next = phead;
	phead->prev = del->prev;

	free(del);
	del = NULL;
}



//头删
void LTPopFront(LTNode* phead)
{
	assert(phead);
	//这个链表除了哨兵位节点还要有有效节点，不能一个有效节点都没有
	assert(phead->next != phead);

	LTNode* del = phead->next;

	del->next->prev = phead;
	phead->next = del->next;

	free(del);
	del = NULL;
}



//在指定位置pos后插入数据
void LTInsert(LTNode* pos, LTDataType x)
{
	assert(pos);
	LTNode* node = ListBuyNode(x);

	node->prev = pos;
	node->next = pos->next;

	pos->next->prev = node;
	pos->next = node;
}



//查找指定位置的数据
LTNode* LTFind(LTNode* phead, LTDataType x)
{
	assert(phead);
	
	//定义一个LTNode的指针用于访问链表
	LTNode* cur = phead->next;

	//遍历每个节点查找
	while (cur != phead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}



//删除指定位置的数据
void LTErase(LTNode* pos)
{
	assert(pos);

	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;

	free(pos);
	pos = NULL;
}



//销毁链表
void LTDestroy(LTNode* phead)
{
	assert(phead);
	LTNode* cur = phead->next;
	LTNode* next;
	while (cur != phead)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	free(phead);
	phead = NULL;
}