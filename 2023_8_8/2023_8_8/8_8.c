#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//题目：写一个二分查找函数
//
//功能：在一个升序数组中查找指定的数值，找到了就返回下标，找不到就返回 - 1.
int bin_search(int arr[], int left, int right, int key)
{
	int flag = 0;//寻找数变量
	int mid;
	while (left <= right)//条件要包含等于使left=right时也能进行判断
	{
		mid = left + (right - left) / 2;//中间元素下标
		if (arr[mid] > key)
		{
			right = mid - 1;
		}//key在左边时right置为mid-1
		else if (arr[mid] < key)
		{
			left = mid + 1;
		}//key在右边时left置为mid+1
		else
		{
			flag = 1;
			break;
		}
	}
	if (flag == 1)
		return mid;
	else
		return -1;
}
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
	int left = 0;//初始左下标
	int right = sizeof(arr) / sizeof(arr[0])-1;//初始右下标
	int key;
	scanf("%d", &key);//要查找的数字
	int s=bin_search(arr, left, right, key);
	printf("%d\n", s);
	return 0;
}