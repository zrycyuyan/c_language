﻿#pragma once
#include<stdio.h>
#include<locale.h>
#include<windows.h>
#include<stdbool.h>
#include<stdlib.h>
#include<time.h>

// 全局变量，保存原始的文本属性
WORD originalAttributes;


#define WALL L'■'//墙体     宽字符可能还是要一些货币等特有的才能打印出宽字符，像我在这里用'Z'就打印不出宽字符
#define BODY L'□'//蛇身
#define FOOD L'●'//食物

#define POS_X 24
#define POS_Y 5

//定义一个宏用于检测此按键是否被按下
#define KEY_PRESS(VK) ((GetAsyncKeyState(VK)&0x1)?1:0)

enum DIRECTION//读取按键状态
{
	UP = 1,
	DOWN,
	LEFT,
	RIGHT
};

enum GAME_STATUS//运行状态
{
	OK,
	END_NORMAL,
	KILL_BY_WALL,
	KILL_BY_SELF
};

//贪吃蛇蛇身节点
typedef struct SnakeNode
{
	int x;
	int y;
	struct SnakeNode* next;
}SnakeNode,*pSnakeNode;

//贪食蛇结构（贪吃蛇完全体）
typedef struct Snake
{
	pSnakeNode _pSnake;//指向贪食蛇头结点的指针（蛇身）
	pSnakeNode _pFood;//指向食物节点的指针（可将食物就当作蛇身的一部分，吃了后蛇身就变长一节）
	int _Score;//总分数
	int _FoodWeight;//单个食物分数
	int _SleepTime;//每走一步停顿时间
	enum DIRECTION _Dir;//获取按键状态描述蛇的方向
	enum GAME_STATUS _Status;//游戏的运行状态：正常、按ESC退出、撞墙死、自杀
	int _HighScore;// 历史最高分
	const char* _FilePath;// 历史最高分记录文件路径
}Snake,*pSnake;

//游戏开始，完成游戏的初始化
void GameStart(pSnake ps);

//设置光标坐标
void SetPos(short x, short y);

//设置贪吃蛇颜色
void color(int c);

//恢复原始的文本颜色属性
void restoreOriginalColor();

//打印欢迎界面
void WelComeToGame();

//创建地图
void CreateMap();

//初始化贪食蛇结构
void InitSnake(pSnake ps);

//游戏的正常运行
void GameRun(pSnake ps);

//蛇的移动
void SnakeMove(pSnake ps);

//判断蛇头到达的坐标处是否是食物
int NextIsFood(pSnake ps, pSnakeNode pnext);

//吃掉食物
void EatFood(pSnake ps, pSnakeNode pnext);

//不吃食物
void NoFood(pSnake ps, pSnakeNode pnext);

//蛇是否撞墙死
void KillByWall(pSnake ps);

//蛇是否自杀
void KillBySelf(pSnake ps);

//打印帮助信息
void PrintHelpInfo();

//按空格暂停以及恢复
void Pause();

//游戏结束后的善后处理
void GameEnd(pSnake ps);