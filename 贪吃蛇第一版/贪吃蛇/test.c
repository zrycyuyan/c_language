#define _CRT_SECURE_NO_WARNINGS
//注意这里我们使用的是旧版的控制台程序，右键单击控制台窗口，在属性中将终端中的默认终端应用程序改为windows终端，若想改回来，选择windows控制台主机（让windows决定）即可
//我还是改回来了，我发现新版的控制台程序更能适应宽字符的打印
#include"Snake.h"
void test()
{
	//Snake snake = { 0 };//创建了贪吃蛇
	////1. 游戏开始 - 初始化游戏
	//GameStart(&snake);

	////蛇的移动
	//void SnakeMove(pSnake ps);

	char ch = 0;
	do
	{
		Snake snake = { 0 };
		GameStart(&snake);
		GameRun(&snake);
		GameEnd(&snake);
		SetPos(20, 18);
		printf("再来一局？(Y/N):");

		//这个获取条件可能有些问题，这是它给我的原因：在 Windows 中，按下键盘上的键，比如 'Y'，实际上会产生两个字符：一个是按键的 ASCII 码（例如，'Y' 的 ASCII 码是 89），另一个是回车键的 ASCII 码（13）。这就是为什么你在第一个 getchar() 中输入 'Y' 时，实际上缓冲区中包含了两个字符，一个是 'Y'，另一个是回车键。
		//因此，在第一个 getchar() 后，缓冲区中仍然存在一个回车键字符。而第二个 getchar() 会读取并消耗掉缓冲区中的这个回车键字符，导致循环无法再次执行。
		//ch = getchar();// 第一次获取用户输入
		//getchar();// 第二次获取回车键

		//这个好像比上面好了，但如果你没吃到任何食物第一次就撞墙的话还是循环不了，不知道为什么
		ch = getchar();  // 获取用户输入的字符
		//清空输入缓冲区，直到遇到换行符
		int c;
		while ((c = getchar()) != '\n' && c != EOF);
	} while (ch == 'Y' || ch == 'y');
	//while (ch != 'N' || ch != 'n');//这也有问题，用这个的话循环不了，不知道为什么
	SetPos(0, 27);
}
int main()
{
	//设置程序适应本地环境
	setlocale(LC_ALL, "");
	srand((unsigned int)time(NULL));

	test();
	return 0;
}
