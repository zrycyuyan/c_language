#define _CRT_SECURE_NO_WARNINGS
#include"SList.h"



//打印链表
void SLPrint(SLNode* phead)
{
	//定义一个SLNode的指针用来访问整个链表
	SLNode* pcur = phead;

	//循环对每个链表节点数据实现打印
	while (pcur != NULL)
	{
		printf("%d->", pcur->data);
		pcur = pcur->next;
	}
	printf("NULL\n");
}



//新建一个节点把数据x放到这个新结点中
SLNode* SLBuyNode(SLDataType x)
{
	SLNode* node = (SLNode*)malloc(sizeof(SLNode));
	node->data = x;
	node->next = NULL;
	return node;
}



//尾插
void SLPushBack(SLNode** pphead, SLDataType x)//注意此处用的是双指针
{
	assert(pphead);
	SLNode* node = SLBuyNode(x);

	if (*pphead == NULL)//当传过来的头结点为空时，直接把node当作头节点即可
	{
		*pphead = node;
		return;
	}

	//正常情况，当传过来的头结点不为空时
	SLNode* pcur = *pphead;//定义了一个SLNode的指针用于访问链表中的所有节点
	while (pcur->next != NULL)
	{
		pcur = pcur->next;
	}//用循环走到最后一个节点
	pcur->next = node;
}



//头插
void SLPushFront(SLNode** pphead, SLDataType x)
{
	assert(pphead);
	SLNode* node = SLBuyNode(x);
	//让新节点与头节点连接起来
	node->next = *pphead;
	//使node处成为新节点
	*pphead = node;
}



//尾删
void SLPopBack(SLNode** pphead)
{
	assert(pphead);
	assert(*pphead);//传过来的节点不能为空

	//当传过来的链表只有一个节点时
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
		return;
	}

	//正常情况，当传过来的链表有多个节点时
	SLNode* prev = NULL;
	SLNode* ptail = *pphead;//定义了两个SLNode型的指针变量，一个用来指向尾节点，一个用来指向尾结点的前一个结点
	while (ptail->next != NULL)
	{
		prev = ptail;
		ptail = ptail->next;
	}
	prev->next = NULL;//将最后一个节点的前一个结点作为尾节点
	free(ptail);
	ptail = NULL;
}




//头删
void SLPopFront(SLNode** pphead)
{
	assert(pphead);
	assert(*pphead);
	SLNode* del = *pphead;//定义一个SLNode型指针变量用于储存首节点地址，然后首节点接可以放心移动
	*pphead = (*pphead)->next;
	free(del);
	del = NULL;
}



//查找第一个为x的节点
SLNode* SLFind(SLNode** pphead, SLDataType x)
{
	assert(pphead);
	SLNode* pcur = *pphead;

	//对链表中的所有节点遍历与x进行比较
	while (pcur)
	{
		if (pcur->data == x)
		{
			return pcur;
		}
		pcur = pcur->next;
	}//若找得到则返回地址
	return NULL;//找不到则返回空地址
}



//在指定位置之前插入数据
void SLInsert(SLNode** pphead, SLNode* pos, SLDataType x)
{
	assert(pphead);
	//传过来的链表和要查找的节点都不能为空
	assert(pos);
	assert(*pphead);
	SLNode* node = SLBuyNode(x);

	//当链表只有一个节点*pphead==pos时
	if (*pphead == pos)
	{
		node->next = *pphead;
		*pphead = node;
	}
	
	//正常情况，链表有多个节点
	SLNode* pcur = *pphead;
	while (pcur->next != pos)
	{
		pcur = pcur->next;
	}
	pcur->next = node;
	node->next = pos;
}



//在指定位置之后插入数据
void SLInsertAfter(SLNode* pos, SLDataType x)//这种情况不需要知道头节点，只需要知道目标节点就可以完成插入
{
	assert(pos);
	SLNode* node = SLBuyNode(x);
	node->next = pos->next;
	pos->next = node;
}



//删除pos节点
void SLErase(SLNode** pphead, SLNode* pos)
{
	assert(pphead);
	assert(pos);
	assert(*pphead);

	//当删除的节点为第一个节点时
	if (*pphead == pos)
	{
		*pphead = NULL;
		free(pos);
		return;
	}

	//正常情况
	SLNode* pcur = *pphead;
	while (pcur->next != pos)
	{
		pcur = pcur->next;
	}
	pcur ->next = pos->next;
	free(pos);
}



//删除pos之后一个节点
void SLEraseAfter(SLNode* pos)
{
	assert(pos && pos->next);
	SLNode* del = pos->next;
	pos->next = del->next;
	free(del);
}



//销毁链表
void SLDesTroy(SLNode** pphead)
{
	assert(pphead);
	SLNode* pcur = *pphead;

	//循环对每个节点进行删除
	while (pcur)
	{
		SLNode* next = pcur->next;
		free(pcur);
		pcur = next;
	}
	*pphead = NULL;
}