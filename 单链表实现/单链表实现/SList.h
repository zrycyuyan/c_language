#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>



typedef int SLDataType;
//定义链表节点的结构
typedef struct SListNode
{
	SLDataType data;
	struct SListNode* next;
}SLNode;



//打印链表
void SLPrint(SLNode* phead);

//尾插
void SLPushBack(SLNode** pphead, SLDataType x);
//头插
void SLPushFront(SLNode** pphead, SLDataType x);
//尾删
void SLPopBack(SLNode** pphead);
//头删
void SLPopFront(SLNode** pphead);

//查找第一个为x的节点
//这里实际上传一级指针就可以了，但我们为了保证接口的一致性，还是传二级指针
SLNode* SLFind(SLNode** pphead, SLDataType x);

//在指定位置之前插入数据
void SLInsert(SLNode** pphead, SLNode* pos, SLDataType x);

//在指定位置之后插入数据
void SLInsertAfter(SLNode* pos, SLDataType x);

//删除pos节点
void SLErase(SLNode** pphead, SLNode* pos);

//删除pos之后一个节点
void SLEraseAfter(SLNode* pos);

//销毁链表
void SLDesTroy(SLNode** pphead);