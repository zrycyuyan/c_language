#define _CRT_SECURE_NO_WARNINGS
#include"SList.h"



void slttest1()
{
	SLNode* plist = NULL;

	//尾插
	SLPushBack(&plist, 1);
	SLPushBack(&plist, 2);
	SLPushBack(&plist, 3);
	SLPushBack(&plist, 4);
	SLPrint(plist);

	//头插
	SLPushFront(&plist, 7);
	SLPushFront(&plist, 8);
	SLPushFront(&plist, 9);
	SLPrint(plist);

	//尾删
	SLPopBack(&plist);
	SLPrint(plist);

	//头删
	SLPopFront(&plist);
	SLPrint(plist);

	//查找第一个为x的节点
	SLPrint(SLFind(&plist, 7));

	//在指定位置之前插入数据
	SLInsert(&plist, SLFind(&plist, 7), 10);
	SLPrint(plist);

	//在指定位置之后插入数据
	SLInsertAfter(SLFind(&plist, 7), 11);
	SLPrint(plist);

	//删除pos节点
	SLErase(&plist, SLFind(&plist, 11));
	SLPrint(plist);

	//删除pos之后一个节点
	SLEraseAfter(SLFind(&plist, 8));
	SLPrint(plist);

	//销毁链表
	SLDesTroy(&plist);
	SLPrint(plist);
}
//void slttest()
//{//定义一些节点，给他们存入数据，用来测试SLPrint函数
//	SLNode* node1 = (SLNode*)malloc(sizeof(SLNode));
//	node1->data = 1;
//	SLNode* node2 = (SLNode*)malloc(sizeof(SLNode));
//	node2->data = 2;
//	SLNode* node3 = (SLNode*)malloc(sizeof(SLNode));
//	node3->data = 3;
//	SLNode* node4 = (SLNode*)malloc(sizeof(SLNode));
//	node4->data = 4;
//
//	//将各个节点串联起来
//	node1->next = node2;
//	node2->next = node3;
//	node3->next = node4;
//	node4->next = NULL;
//
//	//SLPrint(node1);
//	SLNode* plist = node1;
//	SLPrint(plist);
//}



int main()
{
	//slttest();
	slttest1();
	return 0;
}