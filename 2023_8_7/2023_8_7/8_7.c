#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//题目：
//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
int main()
{
	int n, i, j;
	scanf("%d", &n);
	for (i = 1; i <= n; i++)
	{//外层循环控制每一行
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%-3d ", i, j,i * j);
		}//内层循环控制列
		printf("\n");
	}
	return 0;
}