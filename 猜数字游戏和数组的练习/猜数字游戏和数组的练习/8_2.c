#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//猜数字小游戏初版（无限机会）
//void game()
//{
//	int r = 1 + rand() % (100 - 1 + 1);
//	int guess = 0;
//	while (1)
//	{
//		printf("请猜数字：");
//		scanf("%d", &guess);
//		if (r > guess)
//			printf("猜小了\n");
//		else if (r < guess)
//			printf("猜大了\n");
//		else
//		{
//			printf("恭喜你猜对了\n");
//			break;
//		}
//	}
//}
//void menu()
//{
//	printf("****************************\n");
//	printf("******     1.play     ******\n");
//	printf("******     0.exit     ******\n");
//	printf("****************************\n");
//}
//
//int main()
//{
//	int input=0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择->\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("游戏结束\n");
//			break;
//		default:
//			printf("选择错误，重新输入");
//			break;
//		}
//	} while (input);
//	return 0;
//}




//猜数字小游戏进阶版版（5次机会）
void game()
{
	int r = 1 + rand() % (100 - 1 + 1);
	int guess = 0;
	int count = 5;
	int flag = 0;
	while (count)
	{
		printf("你还有%d次机会\n", count);
		count--;
		printf("请猜数字：");
		scanf("%d", &guess);
		if (r > guess)
			printf("猜小了\n");
		else if (r < guess)
			printf("猜大了\n");
		else
		{
			printf("恭喜你猜对了\n");
			flag = 1;
			break;
		}	
	}
	if (flag == 0)
		printf("游戏失败,正确答案是：%d\n", r);
}
void menu()
{
	printf("****************************\n");
	printf("******     1.play     ******\n");
	printf("******     0.exit     ******\n");
	printf("****************************\n");
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择->\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束\n");
			break;
		default:
			printf("选择错误，重新输入");
			break;
		}
	} while (input);
	return 0;
}