#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int i;
//int main()
//{
//    i--;
//    if (i > sizeof(i))
//    {
//        printf(">\n");
//    }
//    else
//    {
//        printf("<\n");
//    }
//    return 0;
//}

//题目：获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列







//题目：输入两个整数，求两个整数二进制格式有多少个位不同
//int main()
//{
//	int i = 0;
//	int j = 0;
//	int d = 0;
//	int conut = 0;
//	printf("请输入两个整数：\n");
//	scanf("%d %d", &i, &j);
//	d = i ^ j;//用^操作符看i和j在二进制中有多少不同位，存放于d中
//	while (d)//求d中1的个数
//	{
//		conut++;
//		d = d & (d - 1);
//	}
//	printf("二进制格式不同位为：%d\n", conut);
//	return 0;
//}

//题目：求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，
//如:153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。
//#include<math.h>
//int main()
//{
//	int i = 0;
//	int conut = 0;
//	int sum = 0;
//	int j = 0;
//	for (i = 0; i <= 100000; i++)
//	{//遍历0-100000的所有数
//		j = i;
//		while (j)
//		{
//			conut++;
//			j = j / 10;
//		}//判断它有几位数
//		j = i;
//		while (j)
//		{
//			sum = sum + pow((j % 10), conut);
//			j = j / 10;
//		}//求各位数字的n次方之和
//		if (sum == i)
//		{
//			printf("%d ");
//		}//判断各位数字的n次方之和和i是否相等，即是否是水仙花数
//		sum = 0;
//		conut = 0;
//	}
//	return 0;
//}

//题目：求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
int main()
{
	int n = 0;
	printf("请输入单个数字：");
	scanf("%d", &n);
	int j;
	j = n;//j用于储存原始的n
	int i = 0;
	int sum = 0;
	for (i = 1; i <= 5; i++)
	{
		sum = sum + n;
		n = n * 10 + j;//n*10后，每一项其实只缺个位的数了，所以此处加一个j
	}
	printf("%d\n", sum);
}