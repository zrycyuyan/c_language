#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>



//fputc
//int main()
//{
//	FILE* fp = fopen("data.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//	fputc('z', fp);
//	fputc('r', fp);
//	fputc('y', fp);
//	fputc('w', fp);
//	fputc('d', fp);
//	fclose(fp);
//	fp = NULL;
//
//	//用fputc向标准输出流中写入数据
//	fputc('z', stdout);
//	fputc('r', stdout);
//	fputc('y', stdout);
//	return 0;
//}



//fgetc
//int main()
//{
//	FILE* fp = fopen("data.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	char ch = fgetc(fp);
//	printf("%c", ch);
//	ch = fgetc(fp);
//	printf("%c", ch);
//	ch = fgetc(fp);
//	printf("%c", ch);
//
//	fclose(fp);
//	fp = NULL;
//
//	//用fgetc从标准输入流中读取数据
//	ch = fgetc(stdin);
//	printf("%c", ch);
//	return 0;
//}



//用fputc和fgetc实现文件拷贝
int main()
{
	FILE* pfread = fopen("data.txt", "r");
	if (fread == NULL)
	{
		perror("fread()::fopen()");
		return;
	}
	FILE* pfwrite = fopen("data1.txt", "w");
	if (pfwrite == NULL)
	{
		perror("pfwrite()::fopen()");
		fclose(pfread);
		pfread = NULL;
		return;
	}

	//char ch;
	int ch;
	//先读
	while ((ch = fgetc(pfread)) != EOF)//注意这里ch=fgetc(pfread)要加一个括号括起来，因为这里的!=优先级要高于赋值运算符
	{
		fputc(ch, pfwrite);//后写
	}

	fclose(pfread);
	pfread = NULL;
	fclose(pfwrite);
	pfwrite = NULL;

	return 0;
}



//鹏哥写的标准代码
//int main()
//{
//	FILE* pfread = fopen("data.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-1");
//		return 1;
//	}
//	
//	FILE * pfwrite = fopen("data1.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen-2");
//		fclose(pfread);
//		pfread = NULL;
//		return 1;
//	}
//
//	//读文件 - 写文件
//	int ch = 0;
//
//	while ((ch = fgetc(pfread)) != EOF)
//	{
//		fputc(ch, pfwrite);
//	}
//
//	//关闭文件
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//
//	return 0;
//}



//fputs
//int main()
//{
//	FILE* fp = fopen("data.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	fputs("zryzdwd\n", fp);
//	fputs("zaici", fp);
//
//	fclose(fp);
//	fp == NULL;
//	return 0;
//}



//fgets
//int main()
//{
//	FILE* fp = fopen("data.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	char arr[20] = { 0 };
//	fgets(arr, 20, fp);//fgets函数是不是遇到“\0”就会停止读取，目前我在调试中好像就是这个结过
//	printf("%s", arr);
//
//	fgets(arr, 20, fp);
//	printf("%s", arr);
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}



//fprintf
//struct S
//{
//	int a;
//	char ch;
//	char name[10];
//};
//
//int main()
//{
//	struct S z = { 0,'z',"zhang" };
//	FILE* fp = fopen("data.txt", "w");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	fprintf(fp,"%d   %c   %s\n", z.a, z.ch, z.name);//fprintf的用法与printf差不多，只是printf默认是针对标准输出流的，所以在使用fprintf前需要在printf所需要的参数基础上添加一个所要输出到的文件
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}



//fscanf
//struct S
//{
//	char name[10];
//	int age;
//};
//
//int main()
//{
//	FILE* fp = fopen("data1.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	struct S z = { 0 };
//	fscanf(fp, "%s   %d", z.name, &z.age);//这个的用法与上面fprintf原理一样
//	//调试发现%s读取到“\n”就停止读取了
//	printf("%s   %d", z.name, z.age);
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}



//sprintf和sscanf
//struct S
//{
//	char name[20];
//	int age;
//	char addr[20];
//};
//
//int main()
//{
//	struct S z = { "zhangruoyu",20,"sichuansheng" };
//	char arr[50] = { 0 };
//	sprintf(arr, "%s %d %s", z.name, z.age, z.addr);//功能：将格式化的字符转换成一个字符串输出到arr中
//	printf("%s\n", arr);
//
//	struct S r = { 0 };
//	sscanf(arr, "%s %d %s", r.name, &r.age, r.addr);//功能：刚好与上面的sprintf相对应，它是从arr中读入格式化字符放到指定位置
//	printf("%s %d %s\n", r.name, r.age, r.addr);
//}



//fwrite
//int main()
//{
//	char name[20] = "zhangruoyu";
//	FILE* fp = fopen("data1.txt", "wb");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	fwrite(name, sizeof(char), 10, fp);//从name处开始读取10个sizeof(char)大小的元素写入到fp文件中
//
//	fclose(fp); 
//	fp = NULL;
//	return 0;
//}



//fread
//int main()
//{
//	FILE* fp = fopen("data1.txt", "rb");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	char name[20];
//	fread(name, sizeof(char), 13, fp);//从fp文件中读取13个大小sizeof(char)的元素放入name处
//	printf("%s\n", name);
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}



//fseek ftell rewind具体功能看书上
//int main()
//{
//	char ch;
//	FILE* fp = fopen("data1.txt", "r");
//	if (fp == NULL)
//	{
//		perror("fopen()");
//		return;
//	}
//
//	fseek(fp, 7, SEEK_SET);//在fp中文件指针移动到从起始位置(SEEK_SET)偏移量为7处
//	ch = fgetc(fp);
//	printf("%c\n", ch);
//
//	int pos = ftell(fp);//返回文件指针当前偏移量         （但我感觉应该是返回的当前第几个元素（这是我通过运行结果看到的））
//	printf("%d\n", pos);
//
//	rewind(fp);//让文件指针重新指向文件起始位置
//	ch = fgetc(fp);
//	printf("%c\n", ch);
//
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}