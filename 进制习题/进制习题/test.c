#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

//输入一个整数 n ，输出该数32位二进制表示中1的个数。其中负数用补码表示。
// //这就是课件上的一道习题，详解见课件
//代码1（只有输入正数结果才对）
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int conut = 0;
//	while (num)
//	{
//		if (num % 2 == 1)
//		{
//			conut++;
//		}
//		num = num / 2;
//	}
//	printf("%d\n", conut);
//}

//代码2：
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int conut = 0;
//	int i = 0;
//	for (i = 0; i <= 32; i++)
//	{
//		if (((num >> i) & 1) == 1)
//		{
//			conut++;
//		}
//	}
//	printf("%d\n", conut);
//	return 0;
//}

//代码3：
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int conut = 0;
//	while (num)
//	{
//		conut++;
//		num = num & (num - 1);
//	}
//	printf("%d\n", conut);
//}

//题目：交换两个变量（不创建临时变量）
// 方1：
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	a = a ^ b;
//	b = a ^ b;//此时b=a^b^b,因为^操作符满足交换律并且b^b=0，0^a=a所以此时b=a;
//	a = a ^ b;//与上面同理a=a^b(交换之前的b)^b(交换之后的b实际为a)
//	printf("%d %d\n", a, b);
//	return 0;
//}

//方2:
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	a = a - b;
//	b = a + b;
//	a = b - a;
//	printf("%d %d\n", a, b);
//	return 0;
//}

//题目：在一个整型数组中，只有一个数字出现一次，其他数组都是成对出现的，请找出那个只出现一次的数字。
//例如：
//数组中有：1 2 3 4 5 1 2 3 4，只有5出现一次，其他数字都出现2次，找出5
int main()
{
	int a[100] = { 0 };
	int n = 0;
	printf("请输入数字的个数：\n");
	scanf("%d", &n);
	int i = 0;
	printf("请输入数字：");
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
	}
	int flat = 0;
	int j = 0;
	for (i = 0; i < n; i++)
	{//第一层循环依次表示输入数字中的每一个数
		for (j = 0; j < n; j++)
		{//第二层循环将每一个数与除这个数之外的其他数作比较，看是否有相等的数
			if ((a[i] == a[j]) && (i != j))
			{
				flat = 1;
				break;
			}
		}
		if (flat == 0)
		{
			printf("单身数为：");
			printf("%d\n", a[i]);
		}
		flat = 0;
	}
	return 0;
}

