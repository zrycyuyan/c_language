#define _CRT_SECURE_NO_WARNINGS
//#include <cstdio>
#include<stdio.h>
//题目：杨氏矩阵
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。  要求：时间复杂度小于O(N);
//int check(int a[3][5], int row, int col, int m)
//{
//	int x = 0;//行
//	int y = col - 1;//列
//	int flag = 0;
//	while (x <= row-1 && y >= 0)
//	{
//		if (m > a[x][y])
//		{
//			x++;
//		}
//		else if (m < a[x][y])
//		{
//			y--;
//		}
//		else
//		{
//			flag = 1;
//			break;
//		}
//	}
//	return flag;
//}
//int main()
//{
//	int a[3][5] = { {2,3,4,5,6},{3,4,5,6,7},{4,5,6,7,8} };
//	int k = 0;
//	printf("请输入要查找的数：");
//	while (~scanf("%d", &k)/*scanf("%d", &k) != EOF不知道为什么这个不行*/)
//	{
//		if (check(a, 3, 5, k))
//		{
//			printf("存在！");
//		}
//		else
//		{
//			printf("不存在!");
//		}
//	}
//	return 0;
//}
//这个代码的参考地址：http://t.csdnimg.cn/cgsKf（思路都在里面）

//int ROW = 3, COL = 4;
//int Check(int a[3][4], int row, int col, int k) {
//	int x = 0;
//	int y = col - 1;
//	while (x <= row - 1 && y >= 0) {
//		if (k > a[x][y]) {    //比我大就向下
//			x++;
//		}
//		else if (k < a[x][y]) {    //比我小就向左
//			y--;
//		}
//		else {
//			return 1;    //相等：找到了
//		}
//	}
//	return 0;    //没有找到
//}
//
//int main() {
//	int a[3][4] = { {1,2,3,4},{5,6,7,8},{9,10,11,12} };//示例
//	int k = 0;    //要查找的数
//
//	printf("请输入你要找的数：\n");
//	while (~scanf("%d", &k)) {
//		if (Check(a, ROW, COL, k)) {
//			printf("找到了！\n");
//		}
//		else {
//			printf("该数不存在！\n");
//		}
//	}
//
//	return 0;
//}

//题目：日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。

//下面0表示平民，1表示凶手
//int main()
//{
//	int a[4] = { 0 };//4个平民
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		a[i] = 1;//对每个平民遍历赋为凶手
//		if (((a[0] != 1) + (a[2] == 1) + (a[3] == 1) + (a[3] != 1)) == 3)//条件为有3个人说真话
//		{
//			break;//满足就跳出循环，a[i]就是凶手
//		}
//		a[i] = 0;//不满足条件则将此人重新置为平民
//	}
//	char b[4] = { 'A','B','C','D' };
//	printf("凶手是%c\n", b[i]);//输出凶手
//	return 0;
//}


//题目：在屏幕上打印杨辉三角。
//1
//1 1
//1 2 1
//1 3 3 1
//……
//int main()
//{
//	int i, j, n = 0;
//	int a[100][100] = { 0 };
//	printf("请输入要打印的行数：");
//	scanf("%d", &n);
//	while (n >= 1 && n <= 100)
//	{
//		for (i = 0; i < n; i++)
//		{
//			a[i][0] = 1;//将前n行的第1列数全部置为1
//		}
//		for (i = 1; i < n; i++)
//		{//从第2行开始
//			for (j = 1; j <= i; j++)
//			{//每行从第2列开始
//				a[i][j] = a[i - 1][j] + a[i - 1][j - 1];//每个元素等于它上一行同一列的元素加上一行此列元素的前面一个元素
//			}
//		}
//		for (i = 0; i < n; i++)
//		{
//			for (j = 0; j < n - i; j++)
//			{
//				printf("  ");
//			}//这个是让屏幕上排版变得更好看的
//			for (j = 0; j <= i; j++)
//			{
//				printf("%d   ", a[i][j]);
//			}
//			printf("\n");
//		}//打印
//		break;
//	}
//	return 0;
//}
//链接：http://t.csdnimg.cn/r2984

//练习使用库函数，qsort排序各种类型的数据
//int int_cmp(void* p1, void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//int main()
//{
//	int arr[] = { 3,4,7,3,6,8,2,9,5,7,1,5,8 };
//	int i = 0;
//	qsort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(arr[0]), int_cmp);
//	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6.
//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int k = 0, m = 0, num1 = 0, num2 = 0, i = 0;
//	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		m = m ^ arr[i];
//	}//遍历所有数异或，得到两个单身狗数不同的二进制位
//	for (i = 0; i < 32; i++)
//	{
//		if (((m >> i) & 1) == 1)
//		{
//			k = i;
//			break;
//		}
//	}//找出单身狗数从哪一位开始不同
//	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		if (((arr[i] >> k) & 1) == 1)
//		{
//			num1 = num1 ^ arr[i];
//		}
//		else
//		{
//			num2 = num2 ^ arr[i];
//		}
//	}//前面已找出两个单身狗书从哪一位开始不同，据此将此二进制位不同的所有数分为两组，两个单身狗数会分别分布于两组中
//	printf("单身狗数为：%d %d\n", num1, num2);
//	return 0;
//}
//参考链接：http://t.csdnimg.cn/ZNccv

//描述
//KiKi想获得某年某月有多少天，请帮他编程实现。输入年份和月份，计算这一年这个月有多少天。
//输入描述：
//多组输入，一行有两个整数，分别表示年份和月份，用空格分隔。
//输出描述：
//针对每组输入，输出为一行，一个整数，表示这一年这个月有多少天。
//int judge_year(int year1)//此函数用于判断年份是否是闰年
//{
//	if (year1 % 4 == 0 && year1 % 100 != 0 || year1 % 400 == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	int year, month;
//	while(scanf("%d %d",&year,&month)!=EOF)
//	{
//		if (judge_year(year))//如果是闰年执行下面
//		{
//			if (month == 2)
//			{
//				printf("29\n");
//			}
//			else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
//			{
//				printf("31\n");
//			}
//			else
//			{
//				printf("30\n");
//			}
//		}
//		else // 如果不是闰年执行下面
//		{
//			if (month == 2)
//			{
//				printf("28\n");
//			}
//			else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
//			{
//				printf("31\n");
//			}
//			else
//			{
//				printf("30\n");
//			}
//		}
//	}
//	return 0;
//}

//在牛客网上的参考代码
//int Judge_Leapyear(int x)//判断是否是闰年
//{
//	if (x % 4 == 0 && x % 100 != 0 || x % 400 == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	int year, month, day;
//	while (scanf("%d%d", &year, &month) != EOF)
//	{
//		if (Judge_Leapyear(year) == 1)//根据返回值进行条件判断
//		{
//			if (month == 2)//将特殊情况放在一种判断的小部分里面
//			{
//				printf("29\n");
//			}
//			else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
//			{
//				printf("31\n");
//			}
//			else
//			{
//				printf("30\n");
//			}
//		}
//		else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)//后再以此输出其他正常情况
//		{
//			printf("31\n");
//		}
//		else if (month == 2)
//		{
//			printf("28\n");
//		}
//		else
//		{
//			printf("30\n");
//		}
//	}
//	return 0;
//}

//将课堂上所讲使用函数指针数组实现转移表的代码，自己实践后，并提交代码到答案窗口。
// 
//转移表
//int add(int x, int y)
//{
//	return x + y;
//}
//int sub(int x, int y)
//{
//	return x - y;
//}
//int mul(int x, int y)
//{
//	return x * y;
//}
//int div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int x, y, input = 0, ret = 0;
//	do
//	{
//		printf("*****************************\n");
//		printf("1.add                   2.sub\n");
//		printf("3.mul                   4.div\n");
//		printf("0.exit                       \n");
//		printf("*****************************\n");
//		printf("请选择：\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = add(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 2:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = sub(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 3:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = mul(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 4:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = div(x, y);
//			printf("ret=%d\n", ret);
//			break;
//		case 0:
//			printf("退出！\n");
//			break;
//		default:
//			printf("选择错误！\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


//用函数指针实现的转移表
//void meun()
//{
//	printf("*****************************\n");
//	printf("1.add                   2.sub\n");
//	printf("3.mul                   4.div\n");
//	printf("0.exit                       \n");
//	printf("*****************************\n");
//}
//int add(int x, int y)
//{
//	return x + y;
//}
//int sub(int x, int y)
//{
//	return x - y;
//}
//int mul(int x, int y)
//{
//	return x * y;
//}
//int div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//
//	int input = 0;
//	int x, y,ret;
//	int (*p[5])(int, int) = { 0,add,sub,mul,div };
//	do
//	{
//		meun();
//		printf("请选择：\n");
//		scanf("%d", &input);
//		if (input <= 4 && input >= 1)
//		{
//			printf("请输入操作数：\n");
//			scanf("%d %d", &x, &y);
//			ret = (*p[input])(x, y);
//			printf("%d\n", ret);
//		}
//		else if (input == 0)
//			printf("退出程序！");
//		else
//			printf("输入有误！重新输入！");
//	} while (input);
//	return 0;
//}


//用回调函数实现的转移表
//void meun()
//{
//	printf("*****************************\n");
//	printf("1.add                   2.sub\n");
//	printf("3.mul                   4.div\n");
//	printf("0.exit                       \n");
//	printf("*****************************\n");
//}
//int calc(int* pf(int, int))
//{
//	int ret = 0;
//	int x, y;
//	printf("请输入两个操作数：\n");
//	scanf("%d %d", &x, &y);
//	ret = (*pf)(x, y);
//	printf("%d\n", ret);
//}
//int add(int x, int y)
//{
//	return x + y;
//}
//int sub(int x, int y)
//{
//	return x - y;
//}
//int mul(int x, int y)
//{
//	return x * y;
//}
//int div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int input = 0;
//	do
//	{
//		meun();
//		printf("请选择：\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			calc(add);
//			break;
//		case 2:
//			calc(sub);
//			break;
//		case 3:
//			calc(mul);
//			break;
//		case 4:
//			calc(div);
//			break;
//		case 0:
//			printf("退出程序！\n");
//			break;
//		default:
//			printf("输入错误！重新输入！\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}

//模仿qsort的功能实现一个通用的冒泡排序
//qsort使用举例
//int int_cmp(const void* p1, const void* p2)
//{
//	return (*(int*)p1 - *(int*)p2);
//}
//int main()
//{
//	int arr[] = { 1,3,4,9,3,0,4,56,7,8 };
//	int i = 0;
//	qsort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(arr[0]), int_cmp);
//	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//
//
int int_cmp(const void* p1, const void* p2)
{//需要自己写的比较两元素大小方式的函数
	return *(int*)p1 - *(int*)p2;
}
void swp(void* p1, void* p2, int size)
{//用来交换两元素的函数，注意是一个字节一个字节的交换
	int tmp, i;
	for (i = 0; i < size; i++)
	{
		tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}
void bubble(void* base, int count, int size, int (*pf)(void*, void*))
{//用冒泡的思想实现是元素升序排列的函数
	int i, j;
	for (i = 0; i < count - 1; i++)
	{
		for (j = 0; j < count - i - 1; j++)
		{
			if (pf((char*)base + j * size, (char*)base + (j + 1) * size)>0)
			{
				swp((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}
int main()
{
	int arr[] = { 6,7,3,8,4,0,2,34,1,6 };
	int i = 0;
	bubble(arr, sizeof(arr) / sizeof(arr[0]), sizeof(arr[0]), int_cmp);
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}