#define _CRT_SECURE_NO_WARNINGS
#include"Snake.h"



//设置贪吃蛇颜色
//void color(int c)
//{
//	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);
//	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), c);
//}
void color(int c)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// 保存原始的文本属性
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
	originalAttributes = consoleInfo.wAttributes;

	// 设置新的文本属性
	SetConsoleTextAttribute(hConsole, c);
}



//恢复原始的文本属性
void restoreOriginalColor()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// 恢复原始的文本属性
	SetConsoleTextAttribute(hConsole, originalAttributes);
}



//设置光标坐标
void SetPos(short x, short y)
{
	COORD pos = { x, y };
	HANDLE hOutput = NULL;
	//获取标准输出的句柄(用来标识不同设备的数值)
	hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	//设置标准输出上光标的位置为pos
	SetConsoleCursorPosition(hOutput, pos);
}



//打印欢迎界面
void WelComeToGame()
{
	//设置颜色
	color(3);

	SetPos(40, 14);//定位光标
	printf("欢迎来到贪食蛇！");
	SetPos(40, 25);
	system("pause");//对程序暂停一下
	system("cls");//清空屏幕
	SetPos(20, 14);
	printf("使用 ↑.↓.←.→.分别控制蛇的移动, F3是加速，F4是减速");
	SetPos(40, 25);
	system("pause");
	system("cls");

	//这里我觉得不用恢复原本颜色属性了，即使我将初始颜色属性都置为这个颜色也不会影响后面贪吃蛇等的打印颜色，因为那些颜色都单独设置过了，只不过后面打印出来的字体颜色都为蓝色了，但这无所谓，
	//恢复原始的文本属性
	//restoreOriginalColor();
}



//创建地图
void CreateMap()
{
	//FOREGROUND_RED、FOREGROUND_GREEN 是 Windows API 中定义的一些常量，可以通过按位或操作（|）进行组合使用，以设置多个属性。
	//设置墙体颜色
	color(FOREGROUND_RED | FOREGROUND_GREEN);
	//上
	SetPos(0, 0);
	int i = 0;
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (i = 1; i < 26; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (i = 1; i < 26; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}

	//恢复原始的文本属性
	restoreOriginalColor();
}



//初始化贪食蛇
void InitSnake(pSnake ps)
{
	pSnakeNode cur = NULL;
	int i = 0;
	for (i = 0; i < 5; i++)
	{//初始蛇身给他五个节点
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("InitSnake()::malloc()");
			return;
		}
		//对每个结点初始化一下下，记录一下下每个节点该出现的位置
		cur->x = POS_X + 2 * i;
		cur->y = POS_Y;
		cur->next = NULL;

		//头插法-----将每个蛇身连接起来
		if (ps->_pSnake == NULL)
		{//倘若没有头，那我便是这唯一的头
			ps->_pSnake = cur;
		}
		else
		{
			cur->next = ps->_pSnake;
			ps->_pSnake = cur;
		}
	}

	//打印蛇身
	//蛇的颜色
	color(FOREGROUND_GREEN);
	cur = ps->_pSnake;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	//恢复原始的文本属性
	restoreOriginalColor();

	//初始化下贪吃蛇结构中的其他内容
	ps->_Status = OK;
	ps->_Score = 0;
	ps->_pFood = NULL;
	ps->_FoodWeight = 10;
	ps->_SleepTime = 200;
	ps->_Dir = RIGHT;

	ps->_HighScore = 0;  // 初始化历史最高分
	// 检查历史最高分文件是否存在，如果不存在则创建
	FILE* file = fopen("highscore.txt", "r");
	if (file == NULL)//不存在highscore.txt文件
	{
		// 创建新文件
		file = fopen("highscore.txt", "w");
		if (file != NULL)
		{
			fprintf(file, "%d", 0);  // 初始时将历史最高分设置为0
			fclose(file);
		}
		else
		{
			perror("GameStart()::fopen()");
		}
	}
	else//存在highscore.txt文件
	{
		fclose(file);
	}
	ps->_FilePath = "highscore.txt";  // 设置历史最高分记录文件路径     
	ps->_ThroughWall = 0;  // 初始化为不穿墙
}



//创建食物
void CreateFood(pSnake ps)
{



	int x = 0;
	int y = 0;
again:
	do
	{//在墙内寻找生成食物的坐标
		x = rand() % 53 + 2;//随机数范围2-54
		y = rand() % 25 + 1;
	} while (x % 2 != 0);//食物的生成坐标要为2的倍数，要不让蛇嵌进墙里咋办，还有蛇只吃到一半食物咋办

	//食物已经在墙内了，现在要看它是否与蛇身重叠
	pSnakeNode cur = ps->_pSnake;//我觉得这一步不能移到goto语句的范围外，因为如果那样当执行goto语句后cur的指向将不会被重置
	while (cur)
	{//遍历判断蛇身坐标是否与所寻找的食物坐标重叠
		if (cur->x == x && cur->y == y)
		{
			goto again;
		}
		cur = cur->next;
	}

	//创建食物节点（可将食物理解成蛇身的一部分）
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("CreateFood()::mallloc()");
		return;
	}
	pFood->x = x;
	pFood->y = y;
	pFood->next = NULL;
	ps->_pFood = pFood;

	//打印食物
	//食物颜色
	color(FOREGROUND_RED);
	SetPos(x, y);
	wprintf(L"%lc", FOOD);
	//恢复原始的文本属性
	restoreOriginalColor();
}



//游戏开始，完成游戏的初始化
void GameStart(pSnake ps)
{
	//调整控制台窗口
	system("mode con cols=100 lines=30");
	system("title 贪食蛇启动！！！");

	//光标影藏掉
	HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	//影藏光标操作
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(hOutput, &CursorInfo);//获取控制台光标信息
	CursorInfo.bVisible = false; //隐藏控制台光标
	SetConsoleCursorInfo(hOutput, &CursorInfo);//设置控制台光标状态

	//打印欢迎界面
	WelComeToGame();

	//创建地图
	CreateMap();

	//初始化贪食蛇
	InitSnake(ps);

	//创建食物
	CreateFood(ps);




	// 读取历史最高分记录文件
	FILE* file = fopen(ps->_FilePath, "r");
	if (file != NULL)
	{
		fscanf(file, "%d", &ps->_HighScore);//从ps->_FilePath文件中读取最高分放到ps->_HighScore种
		fclose(file);
	}
}



//判断蛇头到达的坐标处是否是食物
int NextIsFood(pSnake ps, pSnakeNode pnext)
{
	if (ps->_pFood->x == pnext->x && ps->_pFood->y == pnext->y)
	{//是
		return 1;
	}
	else
	{//不是
		return 0;
	}
}



//吃掉食物
void EatFood(pSnake ps, pSnakeNode pnext)
{
	//头插
	pnext->next = ps->_pSnake;
	ps->_pSnake = pnext;

	//打印蛇身
	//蛇的颜色
	color(FOREGROUND_GREEN);
	pSnakeNode cur = ps->_pSnake;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	//恢复原始的文本属性
	restoreOriginalColor();

	free(ps->_pFood);//此处的食物节点已被pnext记录，并被打印了，所以用后要及时释放
	ps->_Score += ps->_FoodWeight;

	//创建一个新的食物
	CreateFood(ps);

}



//不吃食物
void NoFood(pSnake ps, pSnakeNode pnext)
{

	//头插
	pnext->next = ps->_pSnake;
	ps->_pSnake = pnext;

	//打印蛇身
	//pSnakeNode cur = ps->_pSnake;
	//while (cur->next)//这种写法不太行
	//{
	//	SetPos(cur->x, cur->y);
	//	wprintf(L"%lc", BODY);
	//	cur = cur->next;
	//}

	////记住一点因为没吃食物蛇身不会延长，向前走一节最后一个节点就要被置为“  ”，并且要将最后一个节点释放掉
	//SetPos(cur->x, cur->y);
	//printf("  ");
	//free(cur);
	//cur = NULL;
	//蛇的颜色
	color(FOREGROUND_GREEN);
	pSnakeNode cur = ps->_pSnake;
	while (cur->next->next)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	SetPos(cur->next->x, cur->next->y);
	printf("  ");
	//恢复原始的文本属性
	restoreOriginalColor();
	free(cur->next);
	cur->next = NULL;
}



//蛇是否撞墙死
void KillByWall(pSnake ps)
{
	if (ps->_pSnake->x == 0 || ps->_pSnake->x == 56 || ps->_pSnake->y == 0 || ps->_pSnake->y == 26)
	{
		//ps->_Status = KILL_BY_WALL;

		// 改变穿墙状态
		ps->_ThroughWall = !ps->_ThroughWall;
	}
}



//蛇是否自杀
void KillBySelf(pSnake ps)
{
	pSnakeNode cur = ps->_pSnake->next;
	while (cur)
	{
		if ((ps->_pSnake->x == (cur->x)) && (ps->_pSnake->y == (cur->y)))
		{
			ps->_Status = KILL_BY_SELF;
			return 1;
		}
		cur = cur->next;
	}
}



//蛇的移动
void SnakeMove(pSnake ps)
{
	//定义一个pNext用于记录蛇头将要到达的下一个位置节点
	pSnakeNode pNext = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNext == NULL)
	{
		perror("SnakeMove()::malloc()");
		return;
	}
	pNext->next = NULL;



	// 蛇是否穿墙
	if (ps->_ThroughWall)
	{
		// 根据蛇的方向更新蛇头位置，如果蛇头超出边界，穿越到对应的另一侧
		switch (ps->_Dir)
		{
		case UP:
			pNext->x = (ps->_pSnake->x + 56) % 58;  // 56为边界，58为边界加1
			pNext->y = ps->_pSnake->y;
			break;
		case DOWN:
			pNext->x = (ps->_pSnake->x + 56) % 58;
			pNext->y = ps->_pSnake->y;
			break;
		case LEFT:
			pNext->x = ps->_pSnake->x;
			pNext->y = (ps->_pSnake->y + 26) % 28;  // 26为边界，28为边界加1
			break;
		case RIGHT:
			pNext->x = ps->_pSnake->x;
			pNext->y = (ps->_pSnake->y + 26) % 28;
			break;
		}
	}
	else
	{
		// 蛇不穿墙时的逻辑
		// ...（原有的逻辑）
		//根据按键状态来寻找pNext的坐标位置
		switch (ps->_Dir)
		{//上、下、左、右
		case UP:
			pNext->x = ps->_pSnake->x;
			pNext->y = ps->_pSnake->y - 1;
			break;
		case DOWN:
			pNext->x = ps->_pSnake->x;
			pNext->y = ps->_pSnake->y + 1;
			break;
		case LEFT:
			pNext->x = ps->_pSnake->x - 2;
			pNext->y = ps->_pSnake->y;
			break;
		case RIGHT:
			pNext->x = ps->_pSnake->x + 2;
			pNext->y = ps->_pSnake->y;
			break;
		}
	}
	

	//判断蛇头到达的坐标处是否是食物
	if (NextIsFood(ps, pNext))
	{
		//吃掉食物
		EatFood(ps, pNext);
	}
	else
	{
		//不吃食物
		NoFood(ps, pNext);
	}

	//蛇是否撞墙死
	KillByWall(ps);

	//蛇是否自杀
	KillBySelf(ps);
}



//打印帮助信息
void PrintHelpInfo()
{
	SetPos(64, 15);
	printf("1.不能撞墙，不能咬自己");
	SetPos(64, 16);
	printf("2.使用 ↑.↓.←.→ 分别控制蛇的移动");
	SetPos(64, 17);
	printf("3.F3加速，F4减速");
	SetPos(64, 18);
	printf("4.ESC-退出，空格-暂停");

	SetPos(64, 20);
	printf("@摆烂的小z");
}



//按空格暂停
void Pause()
{
	while (1)//我直接给他一个死循环，让他一直休眠，直到我再按一次空格
	{
		Sleep(100);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
	}
}



//游戏的正常运行
void GameRun(pSnake ps)
{
	//打印帮助信息
	PrintHelpInfo();
	do
	{
		//分数当然要时时更新啦
		SetPos(64, 10);
		printf("得分：%5d", ps->_Score);
		SetPos(64, 11);
		printf("每个食物分数：%2d", ps->_FoodWeight);
		SetPos(64, 12);
		printf("历史最高分：%5d", ps->_HighScore);



		// 比较并更新历史最高分
		if (ps->_Score > ps->_HighScore)
		{
			ps->_HighScore = ps->_Score;

			// 保存历史最高分到文件
			FILE* file = fopen(ps->_FilePath, "w");
			if (file != NULL)
			{
				fprintf(file, "%d", ps->_HighScore);//将新的历史最高分ps->_HighScore写到ps->_FilePath文件中
				fclose(file);
			}
		}



		if (KEY_PRESS(VK_UP) && ps->_Dir != DOWN)//当蛇向下走的时候，肯定不能按下上键啊，那不就自杀了吗，其他也同理
		{
			ps->_Dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->_Dir != UP)
		{
			ps->_Dir = DOWN;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->_Dir != RIGHT)
		{
			ps->_Dir = LEFT;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->_Dir != LEFT)
		{
			ps->_Dir = RIGHT;
		}
		else if (KEY_PRESS(VK_ESCAPE))//按ESC退出
		{
			ps->_Status = END_NORMAL;
			break;
		}
		else if (KEY_PRESS(VK_SPACE))//按空格暂停
		{
			Pause();
		}
		else if (KEY_PRESS(VK_F3))//加速
		{
			if (ps->_SleepTime >= 80)
			{
				ps->_SleepTime -= 30;
				ps->_FoodWeight += 2;
			}
		}
		else if (KEY_PRESS(VK_F4))//减速
		{
			if (ps->_SleepTime <= 320)
			{
				ps->_SleepTime += 30;
				ps->_FoodWeight -= 20;
			}
		}
		Sleep(ps->_SleepTime);
		SnakeMove(ps);
	} while (ps->_Status == OK);
}



//游戏结束后的善后处理
void GameEnd(pSnake ps)
{
	SetPos(20, 12);
	switch (ps->_Status)
	{
	case END_NORMAL:
		printf("您主动退出了游戏");
		break;
	case KILL_BY_SELF:
		printf("您自杀了");
		break;
	/*case KILL_BY_WALL:
		printf("您撞墙了");
		break;*/
	}

	//一局游戏结束，释放蛇身的节点
	pSnakeNode cur = ps->_pSnake;
	while (cur)
	{
		pSnakeNode del = cur;
		cur = cur->next;
		free(del);
	}
	ps->_pSnake = NULL;

	//释放食物节点
	free(ps->_pFood);
	ps->_pFood = NULL;



	//看你愿不愿意再在游戏结束后在打印一次历史最高，我觉得不是特别必要，在右侧已经打印过了
	//// 读取历史最高分
	//int highScore = 0;
	//FILE* file = fopen("highscore.txt", "r");
	//if (file != NULL)
	//{
	//	fscanf(file, "%d", &highScore);
	//	fclose(file);
	//}

	//// 打印历史最高分
	//SetPos(20, 14);
	//printf("历史最高分：%5d", highScore);
}