#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<assert.h>
////模拟实现strlen
////1.计数器方式实现
////size_t my_strlen(const char* str)
////{
////	size_t count = 0;
////	assert(str);
////	while (*str)
////	{
////		count++;
////		str++;
////	}
////	return count;
////}
////int main()
////{
////	char* a = "abcdefg";
////	size_t len = my_strlen(a);
////	printf("%d\n", len);
////}
//
////2.函数递归方式实现（不用中间变量）
////size_t my_strlen(const char* str)
////{
////	assert(str);
////	if (*str == '\0')
////		return 0;
////	else
////		return 1 + my_strlen(str + 1);
////}
////int main()
////{
////	char* a = "abcdefg";
////	size_t len = my_strlen(a);
////	printf("%d\n", len);
////}
//
////3.指针-指针方式实现
////size_t my_strlen(const char* str)
////{
////	assert(str);
////	char* p = str;
////	while (*p)
////	{
////		p++;
////	}
////	return p - str;
////}
////int main()
////{
////	char* a = "abcdefg";
////	size_t len = my_strlen(a);
////	printf("%d\n", len);
////}
//
////模拟实现strcpy
////原始形式
////char* my_strcpy(char* dest, const char* scr)
////{
////	char* ret = dest;
////	assert(dest);
////	assert(scr);
////	while (*dest != '\0')
////	{
////		*dest = *scr;
////		dest++;
////		scr++;
////	}
////	return ret;
////}
////改进形式
////char* my_strcpy(char* dest, const char* scr)
////{
////	char* ret = dest;
////	assert(dest);
////	assert(scr);
////	while (*dest++ = *scr++)
////	{
////		;
////	}
////	return ret;
////}
//
//
////模拟实现strcat
////char* my_strcat(char* dest, const char* scr)
////{
////	char* ret = dest;
////	assert(dest);
////	assert(dest);
////	while (*dest)
////	{
////		dest++;
////	}
////	while (*dest++ = *scr++)
////	{
////		;
////	}
////	return ret;
////}
//
////模拟实现strcmp
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1);
//	assert(str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
////strncpy模拟实现
//char* my_strncpy(char* dest, const char* scr,int size)
//{
//	char* ret = dest;
//	assert(dest);
//	assert(scr);
//	int i = 0;
//	for (i = 0; i < size; i++)
//	{
//		*dest = *scr;
//		if (*dest == '\0')//这种情况是当*scr='\0'时，即已经将scr中的元素全部赋值给了dest,即scr的长度小于size
//			break;
//		dest++;
//		scr++;
//	}
//	*dest = '\0';//强制给的dest后追加一个'\0'
//	return ret;
//}
//
////strncat模拟实现
//char* my_strncat(char* dest, const char* scr, int size)
//{
//	char* ret = dest;
//	int i = 0;
//	assert(dest);
//	assert(scr);
//	while (*dest)
//	{
//		dest++;
//	}
//	for (i = 0; i < size; i++)
//	{
//		*dest = *scr;
//		if (*dest == '\0')//这种情况是当*scr='\0'时，即已经将scr中的元素全部赋值给了dest,即scr的长度小于size
//			break;
//		dest++; 
//		scr++;
//	}
//	*dest = '\0';//强制给的dest后追加一个'\0'
//	return ret;
//}
//
////strncmp模拟实现
////int my_strncmp(const char* str1, const char* str2, int size)
////{
////	int i = 0;
////	assert(str1);
////	assert(str2);
////	for (i = 0; i < size && *str1 == *str2; i++)
////	{
////		if (*str1 == '\0')//在前size个字符中str1和str2相等时
////			return 0;
////		str1++;
////		str2++;
////	}
////	if (i == size)//srr1与str2前size个字符相等
////		return 0;
////	return *str1 - *str2;
////}
//
////strstr模拟实现
////char* my_strstr(const char* str1, const char* str2)
////{
////	char* cp = str1;
////	char* s1;
////	char* s2;
////	if (!*str2)//即当str2为空时
////	{
////		return str1;
////	}
////	while (*cp)
////	{
////		s1 = cp;
////		s2 = str2;
////		while (!(*s1 - *s2) && *s1 && *s2)//即*s2==*s1，并且s2!='\0'时，二者都向后移一位，再对比下一个
////		{
////			s1++;
////			s2++;
////		}
////		if (!*s2)//当比较完后发现s2字符串在s1字符串中能找到时，说明此处cp的地址就是我们要找的
////		{
////			return cp;
////		}
////		cp++;
////	}
////	return NULL;
////}
//
////memcpy模拟
////有问题！！！！！！！！！！
/*void* my_memcpy(void* dest, const void* scr, size_t count)
{
	char* ret = (char*)dest;
	assert(dest);
	assert(scr);
	while (count--)
	{
		*(char*)dest = *(char*)scr;
		(char*)dest = (char*)dest + 1;
		(char*)scr = (char*)scr + 1;
	}
	return ret;
}*///注意对void*类型变量进行解引用或者加减时要先进行强制类型转换
////???
//
////memmove模拟实现
////有问题！！！！！
void* my_memmove(void* dest, const void* scr, size_t count)
{
	char* ret = dest;
	assert(dest);
	assert(scr);
	if (dest <= scr || dest > ((char*)scr + count))//二者有重叠区时，如果目标地址低于起始地址，或者没有重叠区域时，采用将数据从后往前拷贝
	{
		while (count--)
		{
			*(char*)dest = *(char*)scr;
			dest = (char*)dest + 1;
			scr = (char*)scr + 1;
		}
	}
	else//二者有重叠区时，如果目标地址高于起始地址时，采用从后往前拷贝
	{
		(char*)dest = (char*)dest + count - 1;
		(char*)scr = (char*)scr + count - 1;
		while (count--)
		{
			*(char*)dest = *(char*)scr;
			dest = (char*)dest - 1;
			scr = (char*)scr - 1;
		}
	}
	return ret;
}
////???
int main()
{
	/*char* a = "abcdefg";*/ //注意此处的a作为目标拷贝地址一定要可变，就像定义这种char*做目标地址就不行
	/*char a[] = "abbcdefg";
	char a1[20] = "abcdefg";
	char* b = "zhang";
	char* c = "bbc";*/
	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
	int arr1[] = { 0,9,8,7,6 };
	/*size_t len = my_strlen(a);*/
	/*printf("%d\n", len);*/
	/*char* str1 = my_strcpy(a, b);*/
	/*char* str2 = my_strcat(a, b);*/ //大哥对于这个函数你的目标地址要足够容纳追加过来的字符串，记住，所以此时用a就不行
	//char* str3 = my_strcat(a1, b);//这不就可以了嘛！
	/*char* str4 = my_strncpy(a1, b, 4);*/
	/*char* str5 = my_strncat(a1, b, 4);*/
	/*char* str6 = my_strstr(a, c);*/
	/*printf("%s\n", str5);*/
	/*int* ret = my_memcpy(arr, arr1, 5);*/
	int* ret = my_memmove(arr, arr1, 5);
	int i = 0;
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", *(ret + i));
	}
//	int ret = my_strcmp(a, b);
//	if (ret > 0)
//	{
//		printf("a>b\n");
//	}
//	else if (ret < 0)
//	{
//		printf("a<b\n");
//	}
//	else
//	{
//		printf("a=b\n");
//	}
//	return 0;
}





//写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0.
//1.常规判断
//int check()
//{
//	int i = 1;
//	return (*(char*)&i);
//}
//int main()
//{
//	int ret = check();
//	if (ret == 1)
//		printf("小端！\n");
//	else
//		printf("大端！\n");
//	return 0;
//}

//2.共用体判断
//int check2()
//{
//	union un
//	{
//		char ch;
//		int i;
//	}u;
//	u.i = 1;
//	return u.ch;
//}
//int main()
//{
//	int ret = check2();
//	if (ret == 1)
//		printf("小端！\n");
//	else
//		printf("大端！\n");
//	return 0;
//}


