#define _CRT_SECURE_NO_WARNINGS
#include"SeqList.h"
#include"Contact.h"

//测试顺序表函数
//void SLtest()
//{
//	SL s1;
//	SLInit(&s1);
//	SLPushBack(&s1, 1);
//	SLPushBack(&s1, 2);
//	SLPushBack(&s1, 3);
//	SLPushBack(&s1, 4);
//	SLPrint(&s1);
//
//	SLPushFront(&s1, 7);
//	SLPushFront(&s1, 8);
//	SLPushFront(&s1, 9);
//	SLPrint(&s1);
//
//	SLPopBack(&s1);
//	SLPrint(&s1);
//
//	SLPopFront(&s1);
//	SLPrint(&s1);
//
//	SLInsert(&s1, 1, 100);
//	SLPrint(&s1);
//
//	SLErase(&s1, 1);
//	SLPrint(&s1);
//
//	if (SLFind(&s1, 8) == true)
//		printf("找到了！\n");
//	else
//		printf("找不到！\n");
//
//	SLDestory(&s1);
//}



//通讯录实现函数测试
//void contact1()
//{
//	contact con;
//
//	//通讯录初始化
//	ContactInit(&con);
//	
//	ContactAdd(&con);
//	ContactAdd(&con);
//	ContactAdd(&con);
//	ContactShow(&con);
//
//	//ContactDel(&con);
//	//ContactShow(&con);
//
//	//ContactModify(&con);
//	//ContactShow(&con);
//
//	ContactFind(&con);
//	//ContactShow(&con);
//
//	//通讯录销毁
//	ContactDestroy(&con);
//}



//通讯录菜单打印
void menu()
{
	printf("**************************************\n");
	printf("*****1.添加联系人    2.删除联系人*****\n");
	printf("*****3.查找联系人    4.查看通讯录*****\n");
	printf("*****5.修改联系人    0.退出通讯录*****\n");
	printf("**************************************\n");
}



//通讯录完整实现
int main()
{
	//SLtest();
	//contact1();
	int input;
	//定义一个通讯录
	contact con;
	//先初始化一下
	ContactInit(&con);
	do
	{
		menu();
		printf("请选择您的操作：\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			ContactAdd(&con);
			break;
		case 2:
			ContactDel(&con);
			break;
		case 3:
			ContactFind(&con);
			break;
		case 4:
			ContactShow(&con);
			break;
		case 5:
			ContactModify(&con);
			break;
		case 0:
			printf("退出通讯录！\n");
			break;
		default:
			printf("输入有误重新输入！\n");
		}
	} while (input);
	//通讯录用完后要记得销毁
	ContactDestroy(&con);
	return 0;
}