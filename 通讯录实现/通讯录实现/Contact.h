#pragma once

//到底给通讯录的元素分配多大的空间      这样定义之后，若后续数据变大后，方便改动分配相应的空间大小
#define NAME_MAX 100
#define SEX_MAX 10
#define TEL_MAX 15
#define ADDR_MAX 100

struct ContactInfo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
};//此结构体用于储存每个通讯录成员相关信息

//储存每个通讯录成员相关信息，重命名一下，方便
typedef struct ContactInfo CInfo;

//通讯录是基于顺序表实现的，所以此处对前面动态顺序表重命名一下，方便识别这是一个通讯录
typedef struct SeqList contact;

//通讯录的初始化和销毁
void ContactInit(contact* pcon);
void ContactDestroy(contact* pcon);



//添加联系人
void ContactAdd(contact* pcon);
//删除联系人
void ContactDel(contact* pcon);
//修改联系人
void ContactModify(contact* pcon);
//查找指定的联系人
void ContactFind(contact* pcon);



//查看通讯录
void ContactShow(contact* pcon);