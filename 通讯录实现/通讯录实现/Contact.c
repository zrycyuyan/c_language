#define _CRT_SECURE_NO_WARNINGS
#include"SeqList.h"
#include"Contact.h"


//这个通讯录的初始化和销毁都相当于是对前面顺序表套了一个壳，只是换了一个名字让它看起来更像通讯录
void ContactInit(contact* pcon)
{
	SLInit(pcon);
}

void ContactDestroy(contact* pcon)
{
	SLDestory(pcon);
}



void ContactAdd(contact* pcon)
{
	//定义info用来储存通讯录中每个联系人的信息
	CInfo info;
	printf("请输入联系人姓名：\n");
	scanf("%s", info.name);
	printf("请输入联系人性别：\n");
	scanf("%s", info.sex);
	printf("请输入联系人年龄：\n");
	scanf("%d", &info.age);
	printf("请输入联系人电话：\n");
	scanf("%s", info.tel);
	printf("请输入联系人住址：\n");
	scanf("%s", info.addr);

	//上面已经获取到了联系人数据
	//用顺序表中尾插的方式将上面获取到联系人数据插入到已初始化的通讯录中
	SLPushBack(pcon, info);
}



void ContactShow(contact* pcon)
{
	//先打印一个表头提示通讯录的信息
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");

	//打印通讯录中所有成员的信息
	for (int i = 0; i < pcon->size; i++)
	{
		printf("%-4s %-4s %-4d %-4s %-4s\n",
			pcon->a[i].name,
			pcon->a[i].sex,
			pcon->a[i].age,
			pcon->a[i].tel,
			pcon->a[i].addr);
	}
}



//根据姓名查找通讯录中是否存在输入的联系人
int FindByName(contact* pcon, char name[])
{
	for (int i = 0; i < pcon->size; i++)//遍历每一个联系人
	{
		if (strcmp(pcon->a[i].name, name) == 0)//用姓名比较
		{
			return i;
		}
	}
	return -1;
}



void ContactDel(contact* pcon)
{
	char name[NAME_MAX];
	printf("请输入要删除的联系人：\n");
	scanf("%s", name);
	int find = FindByName(pcon, name);//寻找第几个联系人是要删除的联系人
	if (find < 0)
	{
		printf("要删除的联系人不存在！\n");
		return;
	}
	//调用顺序表的删除函数对指定联系人进行删除
	SLErase(pcon, find);
	printf("删除成功！\n");
}



void ContactModify(contact* pcon)
{//逻辑与上面删除的逻辑差不多
	char name[NAME_MAX];
	printf("请输入要修改的联系人：\n");
	scanf("%s", name);
	int find = FindByName(pcon, name);//寻找第几个联系人是要修改的联系人
	if (find < 0)
	{
		printf("要修改的联系人不存在！");
		return;
	}

	//输入要修改的联系人的新的信息
	printf("请输入新的用户名称：\n");
	scanf("%s", pcon->a[find].name);
	printf("请输入新的用户性别：\n");
	scanf("%s", pcon->a[find].sex);
	printf("请输入新的用户年龄：\n");
	scanf("%d", &pcon->a[find].age);
	printf("请输入新的用户电话：\n");
	scanf("%s", pcon->a[find].tel);
	printf("请输入新的用户地址：\n");
	scanf("%s", pcon->a[find].addr);

	//修改成功的话反馈一下
	printf("修改成功！\n");
}



void ContactFind(contact* pcon)
{//逻辑与上面的两个删除和修改也一样
	char name[NAME_MAX];
	printf("请输入要查找的联系人：\n");
	scanf("%s", name);
	int find = FindByName(pcon, name);
	if (find < 0)
	{
		printf("要查找的联系人不存在！");
		return;
	}
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");
	printf("%-4s %-4s %-4d %-4s %-4s\n",
		pcon->a[find].name,
		pcon->a[find].sex,
		pcon->a[find].age,
		pcon->a[find].tel,
		pcon->a[find].addr);
}