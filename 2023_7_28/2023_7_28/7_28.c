#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>


//题目：在屏幕上输出9*9乘法口诀表
//int main()
//{
//	int i, j;
//	for (i = 1; i <= 9; i++)//前面的乘数
//	{
//		for (j = 1; j <= i; j++)//后面的乘数
//		{
//			printf("%d*%d=%d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//题目：计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
#include<math.h>
int main()
{
	int i = 0;
	double sum = 0;
	for (i = 1; i <= 100; i++)
	{
		sum = sum + (pow((-1), i - 1) / i);
	}
	printf("%lf\n", sum);
	return 0;
}