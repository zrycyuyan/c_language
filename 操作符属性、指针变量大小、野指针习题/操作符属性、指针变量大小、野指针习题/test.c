#define _CRT_SECURE_NO_WARNINGS
//题目：获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
#include <stdio.h>
#include<string.h>
//#include<math.h>
//void jinzhi(int n)
//{//用递归打印二进制序列
//	if (n == 1)
//		printf("%d", 1);
//	else if (n == 0)
//		printf("%d", 0);
//	else
//	{
//		jinzhi(n / 2);
//		printf("%d", n % 2);
//	}
//
//}
//int main()
//{
//	int n = 0;
//	printf("请输入一个整数：", n);
//	scanf("%d", &n);
//	printf("n的二进制为：");
//	jinzhi(n);
//	printf("\n");
//	int i = 0;
//	int sum = 0;
//	int j = n;
//	while (j)
//	{//用此循环求出该整数在奇数位二进制上的数的十进制数和，最后再用函数打印二进制即可
//		if (j & 1)//看最右边的奇数位是否为1
//		{
//			sum = sum + pow(2, i);//每一位的权重为2的i次方
//		}
//		j = j >> 2;//执行一次向后移两位跳到下一个奇数位
//		i = i + 1;//每执行一次权重次方数加一
//	}
//	printf("奇数位的二进制为：");
//	jinzhi(sum);
//	printf("\n");
//	sum = 0;
//	i = 0;
//	j = n;
//	while(j)
//	{//用此循环求出该整数在偶数位二进制上的数的十进制数和，最后再用函数打印二进制即可
//		if (j & 2)
//		{
//			sum = sum + pow(2, i);
//		}
//		j = j >> 2;//执行一次向后移两位跳到下一个偶数位
//		i = i + 1;
//	}
//	printf("偶数二进制序列为：");
//	jinzhi(sum);
//	printf("\n");
//}	 


//题目：模拟实现库函数strlen
//int moni(char* p)
//{
//	int conut = 0;
//	int i = 0;
//	while (*(p + i) != '\0')
//	{
//		conut++;
//		i++;
//	}
//	return conut;
//}
//int main()
//{
//	char a[] = "abcdefg";
//	int t = moni(a);
//	printf("%d\n", t);
//}

//题目：实现一个函数，可以左旋字符串中的k个字符。
//例如：
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB
// 方一：自己写的（超复杂）
//int main()
//{
//	int k = 0;
//	scanf("%d", &k);//输入右旋字符个数
//	int i = 0;
//	int j = 0;
//	char a[] = "abcdefghijk";
//	int sz = sizeof(a) / sizeof(a[0]);
//	int t = 0;
//	for (i = 0; i < k; i++)
//	{//用冒泡排序的思想将前面要右旋的字符依次冒泡到最右边
//		for (j = 0; j < sz - 2-i; j++)
//		{
//			t = a[j];
//			a[j] = a[j + 1];
//			a[j + 1] = t;
//		}
//	}
//	int left = sz - k-2+1;
//	int right = sz - 2;
//	while (left <= right)
//	{//再将最右边右旋的字符逆序
//		t = a[left];
//		a[left] = a[right];
//		a[right] = t;
//		left++;
//		right--;
//	}
//	printf("%s\n", a);
//}
// 
//方二：（官方的）
//俺的模仿：
//void leftRound(char* p,int sz,int n)
//{
//	int time = 0, i = 0, j = 0,t=0;
//	time = n % sz;//长度为5的情况下，旋转6、11、16...次相当于1次，7、12、17...次相当于2次，以此类推。
//	for (i = 1; i <= time; i++)
//	{
//		t = *p;//将数组中第一个元素存放到t中
//		for (j = 0; j < sz - 1; j++)
//		{
//			*(p + j) = *(p + j + 1);//将剩余元素均前移一个元素
//		}
//		*(p + j) = t;//将第一个元素的值放到最后一个元素
//		//完成一次自旋
//	}
//}
//官方写的函数：
//void leftRound(char* src, int time)
//{
//	int i, j, tmp;
//	int len = strlen(src);
//	time %= len; //长度为5的情况下，旋转6、11、16...次相当于1次，7、12、17...次相当于2次，以此类推。
//	for (i = 0; i < time; i++) //执行k次的单次平移
//	{
//		tmp = src[0];
//		for (j = 0; j < len - 1; j++) //单次平移
//		{
//			src[j] = src[j + 1];
//		}
//		src[j] = tmp;
//	}
//}
//int main()
//{
//	char a[] =  "abcdefg" ;
//	int m = strlen(a), i = 0;//m算出字符串的长度
//	int k = 0;
//	printf("请输入自旋多少个字符：");
//	scanf("%d", &k);
//	leftRound(a, m,k);
//	for (i = 0; i < m ; i++)
//	{
//		printf("%c", a[i]);
//	}
//	return 0;
//}

//题目：将一个字符串str的内容颠倒过来，并输出。
//输入描述:
//输入一个字符串，可以有空格
//输出描述:
//输出逆序的字符串
//例：
//输入
//I am a student
//输出
//tneduts a ma I
 //方一：
//int main()
//{
//	char a[100] = { '\0' };
//	int i = 0;
//	while ((a[i] = getchar()) != '\n')//getechar()一次接收一个字符
//		i++;
//	i--;//自增完后因为i是我们输入的元素的个数，所以将i当做下标需要减1
//	for (; i >= 0; i--)
//	{//用循环逆序打印我们输入的每一个数
//		printf("%c", a[i]);
//
//	}
//	return 0;
//}
//方二：
//#include<string.h>
//int main()
//{
//	char a[100] = { '\0' };
//	char b[100] = { '\0' };
//	int c = 0;
//	int i = 0;
//	gets(a);//a数组用于存放输入的字符串
//	c = strlen(a);//输入的长度
//	for (i = 0; i < c; i++)
//	{//将a中输入的字符串赋给b并输出b
//		b[i] = a[c - 1 - i];
//		printf("%c", b[i]);
//	}
//}
//#include <stdio.h>
//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}

//题目：写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组。
//void dayin(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//}
//int main()
//{
//	int a[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int sz = sizeof(a) / sizeof(a[0]);
//	dayin(a, sz);
//}

//题目：实现一个对整形数组的冒泡排序
//int main()
//{
//	int a[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	int i = 0;
//	int j = 0;
//	int sz = sizeof(a) / sizeof(a[0]);
//	int t = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (a[j] > a[j + 1])
//			{
//				t = a[j];
//				a[j] = a[j + 1];
//				a[j + 1] = t;
//			}
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", a[i]);
//	}
//	return 0;
//}

//题目：输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。
//void diaohuan(int a[], int b[], int sz)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < sz; i++)
//	{//将所有的奇数先放在b数组前面
//		if (a[i] % 2 == 1)
//		{
//			b[j] = a[i];
//			j++;
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{//再将所有偶数接上面放在b数组的后面
//		if (a[i] % 2 == 0)
//		{
//			b[j] = a[i];
//			j++;
//		}
//	}
//}
//int main()
//{
//	int a[10] = { 0 };
//	int i = 0;
//	int b[10] = { 0 };
//	printf("请输入10个整数：");
//	for (i = 0; i < 10; i++)
//	{//遍历输入
//		scanf("%d", &a[i]);
//	}
//	int sz = sizeof(a) / sizeof(a[0]);
//	diaohuan(a, b, sz);
//	for (i = 0; i < 10; i++)
//	{//遍历输出
//		printf("%d", b[i]);
//	}
//}
//题目：写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
//例如：给定s1 = AABCD和s2 = BCDAA，返回1
//给定s1 = abcd和s2 = ACBD，返回0.
//AABCD左旋一个字符得到ABCDA
//AABCD左旋两个字符得到BCDAA
//AABCD右旋一个字符得到DAABC

void leftRound(char* p, int sz, int k)
{//就是前面的字符串自旋函数
	int i, j,t;
	int time = k % sz;
	for (i = 0; i < time; i++)
	{
		t = *p;
		for (j = 0; j < sz; j++)
		{
			*(p + j) = *(p + j + 1);
		}
		*(p + j) = t;
	}
}
int panduan(char* a1, char* b1,int sz)
{
	int m,flag=0;
	for (m = 0; m < sz; m++)
	{//调用循环，a每次自旋一个字符，直到所有字符全部自旋，判断每次自旋后a,b是否相等
		if (strcmp(a1, b1) == 0)
		{//自旋后相等flag置1
			flag = 1;
			break;
		}
		leftRound(a1, sz, 1);
	}
	return flag;
}
int main()
{
	char a[] = "aabcd";
	char b[] = "bcdaa";
	int len = strlen(a);//计算a,b数组的长度
	int z=panduan(a, b, len);
	printf("%d\n", z);
	return 0;
}//运行结果不对