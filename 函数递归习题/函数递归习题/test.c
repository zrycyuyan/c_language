#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//题目：递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1

//非递归方式：
//int main()
//{
//	int i=1, j=1;
//	int a=1;
//	int n = 0;
//	int t = 0;
//	scanf("%d", &n);
//	while (n-2)
//	{
//		a = i + j;//计算第n个斐波拉契数
//		t = j;
//		j = a;
//		i = t;//将i和j均向前走一位
//		n--;
//	}
//	printf("%d\n", a);
//	return 0;
//}

//递归方式：
//int digui(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return digui(n - 1) + digui(n - 2);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int t = digui(n);
//	printf("%d\n", t);
//	return 0;
//}

//题目：编写一个函数实现n的k次方，使用递归实现。
//int dayin(int n, int k)
//{
//	if (k == 1)
//		return n;
//	else
//		return n * dayin(n, k - 1);//因为n的k次方可写为n*n的(k-1)次方，n的(k-1)次方可写为n*n的(k-2)次方···
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int t = dayin(n, k);
//	printf("%d\n", t);
//	return 0;
//}

//题目：写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19
//int qiuhe(int n)
//{
//	int sum = 0;
//	if (n < 10)
//		return n;
//	else
//	{
//		sum = n % 10 + qiuhe(n/10);//注意此处n/10才是下一个函数的参数，别搞错了
//		return sum;
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int t = qiuhe(n);
//	printf("%d\n", t);
//	return 0;
//}

//题目：递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//非递归：
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ji = 1;
//	while (n)
//	{
//		ji = ji * n;
//		n--;
//	}
//	printf("%d\n", ji);
//	return 0;
//}

//递归：
//int jiesheng(int n)
//{
//	if (n <= 1)
//		return 1;
//	else
//		return n * jiesheng(n - 1);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int t = jiesheng(n);
//	printf("%d\n", t);
//	return 0;
//}

//题目：递归方式实现打印一个整数的每一位
void dayin(int n)
{
	if (n < 10)
		printf("%d ",n);
	else
	{
		dayin(n / 10);
		printf("%d ", n % 10);
	}
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	dayin(n);
	return 0;
}