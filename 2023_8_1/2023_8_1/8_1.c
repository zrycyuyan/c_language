#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//题目：编写一个程序，从用户输入中读取10个整数并存储在一个数组中。然后，计算并输出这些整数的平均值。
//int main()
//{
//	int i = 0, a[10], sum = 0;
//	double aver = 0;
//	printf("请输入十个整数：");
//	for (i = 0; i <= 9; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	for (i = 0; i <= 9; i++)
//	{
//		sum = sum + a[i];
//	}
//	aver = sum / 10.0;
//	printf("平均数为：%lf\n", aver);
//	return 0;
//}






//题目：KiKi有一个矩阵，他想知道转置后的矩阵（将矩阵的行列互换得到的新矩阵称为转置矩阵），请编程帮他解答。
//
//输入描述：
//第一行包含两个整数n和m，表示一个矩阵包含n行m列，用空格分隔。(1≤n≤10, 1≤m≤10)
//
//从2到n + 1行，每行输入m个整数（范围 - 231~231 - 1），用空格分隔，共输入n * m个数，表示第一个矩阵中的元素。
//
//输出描述：
//输出m行n列，为矩阵转置后的结果。每个数后面有一个空格。
//int main() {
//    int i, j, n, m, a[10][10],b[10][10];//两个数组一个存放原数组还未改变时的元素，另一个存放改变行列后的元素
//    //两个数组元素可以定义大一点，至少要让输入的行列能正常运行，不一定要卡死
//    scanf("%d %d", &n, &m);
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < m; j++)
//        {
//            scanf("%d", &a[i][j]);
//            b[j][i] = a[i][j];//先给a数组遍历赋初值，再将a数组中元素给交换行列的b数组
//        }
//    }
//    for (j = 0; j < m; j++)
//    {
//        for (i = 0; i < n; i++)
//        {
//            printf("%d ", b[j][i]);
//        }
//        printf("\n");
//    }
//    return 0;
//}