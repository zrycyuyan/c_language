#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//题目：将数组A中的内容和数组B中的内容进行交换。（数组一样大）
//int main()
//{
//	int a[10] = { 0,1,2,3,4,5,6,7,8,9 }, b[10] = { 9,8,7,6,5,4,3,2,1,0 }, c[10] = { 0 },i=0;
//	for (i = 0; i <= 9; i++)
//	{
//		c[i] = a[i];
//		a[i] = b[i];
//		b[i] = c[i];
//	}
//	for (i = 0; i <= 9; i++)
//	{
//		printf("a[%d]=%d ", i, a[i]);
//	}
//	printf("\n");
//	for (i = 0; i <= 9; i++)
//	{
//		printf("b[%d]=%d ", i, b[i]);
//	}
//	printf("\n");
//}






//给定两个数，求这两个数的最大公约数
//
//例如：
//
//输入：20 40
//
//输出：20
//int main()
//{
//	int a[50] = { 0 }, i = 0, j = 0, n, m, t, max = 0;
//	scanf("%d %d", &n, &m);
//	if (n > m)
//	{
//		t = n;
//		n = m;
//		m = t;
//	}//始终让n<=m
//	for (i = 2; i <= n; i++)
//	{
//		if ((n % i == 0) && (m % i == 0))
//		{
//			a[j] = i;
//			j++;
//		}//将n和m的公约数放在a数组中
//	}
//	for (i = 0; i <= n; i++)
//	{
//		if (a[i] > max)
//		{
//			max = a[i];
//		}
//	}//遍历找出最大公约数
//	printf("n和m的最大公约数为：%d\n", max);
//}






//题目：打印1000年到2000年之间的闰年
//int main()
//{
//	int i;
//	for (i = 1000; i <= 2000; i++)
//	{
//		if (((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0))
//		{
//			printf("%d ", i);
//		}
//	}
//	printf("\n");
//}



//题目：KiKi想知道已经给出的三条边a，b，c能否构成三角形，如果能构成三角形，判断三角形的类型（等边三角形、等腰三角形或普通三角形）。
//输入描述：
//题目有多组输入数据，每一行输入三个a，b，c(0 < a, b, c < 1000)，作为三角形的三个边，用空格分隔。
//输出描述：
//针对每组输入数据，输出占一行，如果能构成三角形，等边三角形则输出“Equilateral triangle!”，等腰三角形则输出“Isosceles triangle!”，其余的三角形则输出“Ordinary triangle!”，反之输出“Not a triangle!”。
#include <stdio.h>

int main() {
    int a, b, c;
    while (scanf("%d %d %d", &a, &b, &c) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 
        if ((a + b > c) && (a + c > b) && (b + c > a))
        {//三角形两边之和大于第三边
            if ((a == b) && (b == c))//三边都相等
                printf("Equilateral triangle!\n");
            else if (((a == b) || (b == c) || (a == c)) && ((a != b) || (a != c) || (b != c)))//只有两边相等
                printf("Isosceles triangle!\n");
            else
                printf("Ordinary triangle!\n");
        }
        else
            printf("Not a triangle!\n");

    }
    return 0;
}