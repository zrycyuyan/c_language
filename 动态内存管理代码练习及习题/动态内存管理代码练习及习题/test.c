#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>



//malloc练习
//int main()
//{
//	//malloc在堆上开辟动态内存
//	int* p = (int*)malloc(10 * sizeof(int));
//
//	//若开辟失败，则返回并打印错误信息
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//开辟成功，则对每个元素赋值并打印
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	//用完后释放掉动态内存
//	free(p);
//	p = NULL;
//	return 0;
//}



//calloc实现
//int main()
//{
//	//malloc在堆上开辟动态内存
//	//int* p = (int*)malloc(10 * sizeof(int));
//	//calloc与maloc效果大概差不多，只是calloc会将开辟的每一个字节初始化为0
//	int* p = (int*)calloc(10, sizeof(int));
//
//	//若开辟失败，则返回并打印错误信息
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//开辟成功，则对每个元素赋值并打印
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	//用完后释放掉动态内存
//	free(p);
//	p = NULL;
//	return 0;
//}



//realloc实现
//int main()
//{
//	//malloc在堆上开辟动态内存
//	int* p = (int*)malloc(10 * sizeof(int));
//
//	//若开辟失败，则返回并打印错误信息
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//realloc调整开辟内存大小
//	int* tmp = (int*)realloc(p, 20 * sizeof(int));
//	//若开辟失败，则返回并打印错误信息
//	if (tmp == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	//开辟成功，则对每个元素赋值并打印
//	p = tmp;
//	tmp = NULL;
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 20; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	//用完后释放掉动态内存
//	free(p);
//	p = NULL;
//	return 0;
//}








//int* getConcatenation(int* nums, int numsSize, int* returnSize) {
//	int i;
//	*returnSize = 2 * numsSize;
//	nums = (int*)realloc(nums, sizeof(int) * (*returnSize));
//	//int* tmp=(int*)realloc(nums,(sizeof(int))*(*returnSize));
//	// /*if(tmp==NULL)
//	// {
//	//     perror("realloc");
//	//     return NULL;
//	// }
//	// nums=tmp;
//	// free(tmp);*/
//	for (i = 0; i < numsSize; i++)
//	{
//
//		nums[i + numsSize] = nums[i];
//	}
//	return nums;
//}
//int* getConcatenation(int* nums, int numsSize, int* returnSize) {
//    int i;
//    *returnSize = 2 * numsSize;
//    nums = (int*)realloc(nums, sizeof(int) * (*returnSize));
//    //int* tmp=(int*)realloc(nums,sizeof(int)*(*returnSize));
//    // if(tmp==NULL)
//    // {
//    //     perror("reallocc");
//    //     return;
//    // }
//    // nums=tmp;
//    // free(tmp);
//    for (i = 0; i < numsSize; i++)
//    {
//
//        nums[i + numsSize] = nums[i];
//    }
//    return nums;
//}
//
//int main()
//{
//	int nums[] = { 1,2,3 };
//	int numsSize = 3;
//	int returnSize;
//	getConcatenation(nums, numsSize, &returnSize);
//	return 0;
//}//这个代码在这有问题，具体去看我在力扣上的做题记录，里面有注释







//使用malloc函数模拟开辟一个3*5的整型二维数组，开辟好后，使用二维数组的下标访问形式，访问空间。
int main()
{
	int i = 0,j;
	//定义一个数组指针，其内有3个元素，每个元素大小为5个字节
	int(*p)[3];
	//动态开辟数组指针的大小
	p = (int*)malloc(3 * 5 * sizeof(int));

	//遍历赋值
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			p[i][j] = i + j;
		}
	}

	//遍历打印
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 5; j++)
		{
			printf("%d ", p[i][j]);
		}
	}
}