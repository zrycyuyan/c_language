#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//题目：
//输入两个升序排列的序列，将两个序列合并为一个有序序列并输出。
//
//输入描述：
//输入包含三行，
//
//第一行包含两个正整数n, m，用空格分隔。n表示第二行第一个升序序列中数字的个数，m表示第三行第二个升序序列中数字的个数。
//
//第二行包含n个整数，用空格分隔。
//
//第三行包含m个整数，用空格分隔。
//输出描述：
//输出为一行，输出长度为n + m的升序序列，即长度为n的升序序列和长度为m的升序序列中的元素重新进行升序序列排列合并。
//int main()
//{
//	int a[1000], b[1000], c[1000], n, m, i, j, t;
//	scanf("%d %d", &n, &m);
//	for (i = 0; i < n; i++)
//		scanf("%d", &a[i]);//为a数组赋初值
//	for (i = 0; i < m; i++)
//		scanf("%d", &b[i]);//为b数组赋初值
//	for (i = 0; i < n; i++)
//		c[i] = a[i];
//	for (j = n, i = 0; j < (n + m); j++, i++)
//		c[j] = b[i];//将a,b数组中元素都放到c数组中
//	for (i = 1; i < (m + n); i++)
//	{
//		for (j = 1; j <= (m + n - i); j++)
//		{
//			if (c[j - 1] > c[j])
//			{
//				t = c[j];
//				c[j] = c[j - 1];
//				c[j - 1] = t;
//			}
//		}
//	}//对c数组中元素用冒泡排序使其元素升序排列
//	for (i = 0; i < (m + n); i++)
//	{
//		printf("%d ", c[i]);//遍历输出c数组中对应元素
//	}
//}


//题目：
//实现一个函数is_prime，判断一个数是不是素数。
//
//利用上面实现的is_prime函数，打印100到200之间的素数。
//int is_prime(int n)
//{
//	int i,flag=1;
//	for (i = 2; i < n; i++)
//	{
//		if (n % i == 0)
//		{
//			flag = 0;
//			break;
//		}
//	}
//	return flag;
//}
//int main()
//{
//	int i;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	printf("\n");
//}





//题目：
//实现函数判断year是不是润年。
//int year(int nian)
//{
//	int flag = 0;
//	if ((nian % 4 == 0) && (nian % 100 != 0) || (nian % 400 == 0))
//		flag = 1;
//	return flag;
//}
//int main()
//{
//	int nian;
//	scanf("%d", &nian);
//	if (year(nian))
//		printf("%d是闰年\n",nian);
//	else
//		printf("%d不是闰年\n",nian);
//	return 0;
//}





//题目：
//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
//void init(int a[])
//{
//	int i;
//	for (i = 0; i < 10; i++)
//	{
//		a[i] = 0;
//	}
//	return;
//}
//void print(int a[])
//{
//	int i;
//	for (i = 0; i < 10; i++)
//		printf("%d ", a[i]);
//}
//void reverse(int a[])
//{
//	int sz = 10; /*sizeof(a) / sizeof(a[0])*///这里还是有点问题进入函数后sizeof(a)的结果为8而不是正确的40；
//	int i, left=0, right=sz-1, t;
//	for (i = 0; i < (sz / 2); i++)
//	{//执行循环，只需一半元素即可，因为两两元素交换刚好sz/2趟
//		if (left < right)
//		{
//			t = a[left];
//			a[left] = a[right];
//			a[right] = t;
//		}//第一个元素和最后一个元素交换，第二个元素和倒数第二个元素交换······
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int a[10] = { 1,2,3,4,5,6,7,8,9,0 }, i;
//	/*for (i = 0; i < 10; i++)
//		scanf("%d", &a[i]);*/
//	/*init(a);*/
//	reverse(a);
//	for (i = 0; i < 10; i++)
//		printf("%d ", a[i]);
//	/*printf("%d\n", sizeof(a));*/
//	/*print(a);*/
//	
//}








